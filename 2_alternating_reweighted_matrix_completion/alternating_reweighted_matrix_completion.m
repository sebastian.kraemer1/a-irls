function [Y,Z,info] = alternating_reweighted_matrix_completion(problem,solution,meta_para)

close all

    function suggest_default(field,default)
        if ~isfield(meta_para,field)
            meta_para.(field) = default;
        end
    end

%% Parameter defaults (preferences)

% weight parameter p
suggest_default('weight_p',0);
if meta_para.weight_p ~= 0
    warning('not using rank weights (p = 0) but p = %f',meta_para.weight_p);
end

% preferences
suggest_default('iter_max',10000000); % will break earlier by itself
suggest_default('print_interval',1000);
suggest_default('plot_interval',1000);
suggest_default('f_gamma',1.01);

% breaking criteria
suggest_default('rel_gamma_min',1e-10);
suggest_default('nres_min',0.8e-6);

suggest_default('breaking_conditions','elaborate');
suggest_default('rel_sigma_min',1e-10);
suggest_default('sigma_gamma_factor',10);
suggest_default('rel_y_min',1e-12);

% heuristic, well working choices
suggest_default('omega',0.5);
suggest_default('marginal_factor',1e-12);
suggest_default('gamma_start_factor',10);

%% read problem setting
n = problem.n;
P = problem.P; % sampling points
y = problem.y;

Ytrue = solution.Ntrue.Y;
Ztrue = solution.Ntrue.Z;

% magnitudes
ell = size(P,1);
norm_L = sqrt(ell); % = norm(L,'fro');
norm_y = norm(y);
scaling_L = prod(n)/norm_L^2;

%% rank (why does this need to be one higher than the true rank!?)
suggest_default('YZrank',min(min(n),1+floor((sum(n)/2 - sqrt(sum(n)^2 - 4*ell)/2))));

%% Create directories
[dir,dl] = gen_sampling_directories(P,n);

%% svd of Ntrue
% ensure Ztrue is orthogonal
[U,SZ,V] = svd(Ztrue,'econ');
Ytrue = Ytrue*U*SZ; Ztrue = V'; % not yet singular values of Xtrue
sigma_true = svd(Ytrue,0);

ind_stable_true = sigma_true/norm(sigma_true) > meta_para.marginal_factor;
num_stable_sv_true = sum(ind_stable_true);
prod_stable_sv_true = prod(sigma_true(ind_stable_true));

norm_Xtrue = norm(sigma_true);

%% for method relaxed
omega = meta_para.omega;

%% init X = YZ
R = meta_para.YZrank;
Y = randn(n(1),R);
Z = randn(R,n(2));

gamma = meta_para.gamma_start_factor*norm_y^2*prod(n)/norm_L^2;
Sigma = [];
Gamma = [];
Res_y = [];

%% colors for plot
blue = [0,83,159]/255;
teal = [0,152,161]/255;
bord = [161,16,53]/255;
viol = [97,33,88]/255;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
tic

% ensure Z is orthogonal
[U,SZ,V] = svd(Z,'econ');
Y = Y*U*SZ; Z = V'; % not yet singular values of X = YZ

RZero = zeros(R,1);
[sub1,b1,Wsub1] = deal(cell(1,n(1)));

for i = 1:n(1)
    sub1{i} = dir(i,1:dl(i,1),1);
    Wsub1{i} = [sub1{i},ell+1:ell+R];
    b1{i} = [y(sub1{i}); RZero];
end
[sub2,b2,Wsub2] = deal(cell(1,n(2)));
for i = 1:n(2)
    sub2{i} = dir(i,1:dl(i,2),2);
    Wsub2{i} = [sub2{i},ell+1:ell+R];
    b2{i} = [y(sub2{i}); RZero];
end  

for iter = 1:meta_para.iter_max    
    scaling_W = omega^2*gamma;
    
    %% ortho Y
    [U,S,VY] = svd(Y,'econ');
    Z = S*VY'*Z;
    Y = U;
    sigma = diag(S);
    
    %% update Z
    WA = 1/sqrt(scaling_L)*sqrt(scaling_W)*diag((sigma.^2+gamma).^(meta_para.weight_p/4-1/2));
    Y_P1catWA0 = [Y(P(:,1),:); WA; zeros(1,R)];
      
    for i = 1:n(2)
        Z(:,i) = Y_P1catWA0(Wsub2{i},:) \ b2{i};
    end
       
    %% ortho Z
    [UZ,S,V] = svd(Z,'econ');
    Z = V';
    Y = Y*UZ*S;
    sigma = diag(S);
    
    %% update Y
    WA = 1/sqrt(scaling_L)*sqrt(scaling_W)*diag((sigma.^2+gamma).^(meta_para.weight_p/4-1/2));
    Z_P2trcatWA0 = [Z(:,P(:,2))'; WA; zeros(1,R)];
    
    for i = 1:n(1)
        Y(i,:) = Z_P2trcatWA0(Wsub1{i},:) \ b1{i};
    end
    
    %% evaluate progress    
    sigma = svd(Y,'econ');
    X_P = sum(Y(P(:,1),:).*Z(:,P(:,2))',2); 
    
    ind_stable = sigma > sqrt(gamma);
    last_stable_sv = sigma(find(~ind_stable,1));
    
    % nres_X_true = norm(X-Xtrue,'fro')
    sY = Ytrue'*Y;
    sZ = Ztrue*Z';
    nres_X_true = sqrt(sigma(:)'*sigma(:) - 2*sY(:)'*sZ(:) + sigma_true(:)'*sigma_true(:));
    
    nres_y = norm(X_P-y);
    nres_y_scaled = nres_y*sqrt(scaling_L);
    rel_nres_y = nres_y/norm_y;
    
    f_scaled = sqrt(scaling_L*nres_y^2 + scaling_W*(log(prod(sigma.^2+gamma)) - length(sigma)*log(gamma)));
    
    num_stable_sv = sum(ind_stable);
    prod_stable_sv = prod(sigma(ind_stable));
    
    %% print
    print_input = {iter,toc,sqrt(gamma),last_stable_sv/sqrt(gamma),num_stable_sv,num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true/norm_Xtrue};
    if ~mod(iter,meta_para.print_interval)
        print_progress(print_input{:});
    end
        
    %% plot
    if ~mod(iter,meta_para.plot_interval)
        Sigma = [Sigma; sigma(:)']; %#ok<AGROW>
        Gamma = [Gamma; sqrt(gamma)]; %#ok<AGROW>
        
        hold off;
        
        semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Gamma(:),'-','color',teal);
        hold on;
        
        for i = 1:size(Sigma,2)
            semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Sigma(:,i),'-','color',blue);
        end
        for i = 1:num_stable_sv_true
            semilogy([meta_para.plot_interval,iter],[sigma_true(i),sigma_true(i)],'--','color',viol);
        end
        
        Res_y = [Res_y; nres_y_scaled]; %#ok<AGROW>
        semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Res_y(:),':','color',bord);

        drawnow
    end
    
    %% consider break
    switch meta_para.breaking_conditions
        case 'plain'
            if sqrt(gamma)/norm(sigma) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
                print_progress(print_input{:});
                break;
            end
        case 'elaborate'
            if sigma(num_stable_sv_true+1) < meta_para.rel_sigma_min*norm(sigma_true) && rel_nres_y < meta_para.rel_y_min || sqrt(gamma)/norm(sigma) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
                print_progress(print_input{:});
                break;
            end   
            if num_stable_sv > num_stable_sv_true && sigma(num_stable_sv_true+1) > meta_para.sigma_gamma_factor*sqrt(gamma)
                fprintf('Improvement is not to be expected. Stopping.\n');
                print_progress(print_input{:});
                break;
            end
    end
    
    %% decrease gamma
    gamma = gamma / meta_para.f_gamma;

end

info = struct;
info.gamma = gamma;
info.meta_para = meta_para;
info.time = toc;
info.iter = iter;

end

function print_progress(iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue)

        fprintf('iter: %4d, time: %2.2f, fsqrt(gamma): %.2e, quot_gamma_stable: %.2e, num_stable: %d/%d, quot_prod_stable: %.2e, f_sc: %.2e, nres_y_sc %.2e, rel_nres_X_true: %.2e \n',...
            iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue);
        
end



