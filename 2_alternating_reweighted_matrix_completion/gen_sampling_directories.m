function [dir,dl] = gen_sampling_directories(P,n)

dl = zeros(max(n),2);
ell = size(P,1);
dir = zeros(max(n),floor(5*ell/max(n)),2);
for q = 1:ell
    dl(P(q,1),1) = dl(P(q,1),1) + 1;
    dir(P(q,1),dl(P(q,1),1),1) = q;
    dl(P(q,2),2) = dl(P(q,2),2) + 1;
    dir(P(q,2),dl(P(q,2),2),2) = q;
end