function test_alternating_reweighted_matrix_completion()

%% parameters

% problem:
problem_setting.n = [20,20]; 
problem_setting.Ltype = 'sampling';
problem_setting.ell_fct = @(n,r,vol) 1.25*(sum(n)*r-r^2);

% reference solution:
solution_setting.rXtrue = 3;

% important meta parameters
meta_para.f_gamma = 1.001;

%% get problem
[problem,solution] = get_network_matrix_completion_prob_sol(problem_setting,solution_setting);




%% run (p = 0)
meta_para.weight_p = 0;
[Y_alg,Z_alg] = alternating_reweighted_matrix_completion(problem,solution,meta_para);

accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_X_true] = evaluate_network_matrix(problem.P,Y_alg,Z_alg,solution.Ntrue.Y,solution.Ntrue.Z,problem.y,accuracy);
fprintf('p = 0 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_X_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_X_true);



%% run (p = 1)
meta_para.weight_p = 1;
[Y_alg,Z_alg] = alternating_reweighted_matrix_completion(problem,solution,meta_para);

accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_X_true] = evaluate_network_matrix(problem.P,Y_alg,Z_alg,solution.Ntrue.Y,solution.Ntrue.Z,problem.y,accuracy);
fprintf('p = 1 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_X_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_X_true);
