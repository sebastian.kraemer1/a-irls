function [success,diff,quot,rel_nres_y,rel_nres_X_true,sigma,sigma_true] = evaluate_network_matrix(P,Y,Z,Ytrue,Ztrue,y,accuracy)

success = false;

%% rank and singular values with respect to accuracy
[num_stable_sv,prod_stable_sv,sigma] = eval_this(Y,Z,accuracy);
[num_stable_sv_true,prod_stable_sv_true,sigma_true] = eval_this(Ytrue,Ztrue,accuracy);

%% difference and quotient
diff = num_stable_sv - num_stable_sv_true;
quot = prod_stable_sv / prod_stable_sv_true;

%% residuals
X_P = sum(Y(P(:,1),:).*Z(:,P(:,2))',2);
rel_nres_y = norm(X_P-y)/norm(y);

sY = Ytrue'*Y;
sZ = Ztrue*Z';
nres_X_true = sqrt(sigma(:)'*sigma(:) - 2*sY(:)'*sZ(:) + sigma_true(:)'*sigma_true(:));
rel_nres_X_true = nres_X_true/norm(sigma_true);

%% check whether success (1) or improvement (2)
if rel_nres_y < accuracy
    if diff == 0 && quot > 0.98 && quot < 1.005
        success = true;
    end
    if diff < 0 || diff == 0 && quot <= 0.98
        success = 2;
    end
end

end

function [num_stable_sv,prod_stable_sv,sigma] = eval_this(Y,Z,accuracy)

% ensure Ztrue is orthogonal
[U,SZ,~] = svd(Z,0);
Y = Y*U*SZ;
sigma = svd(Y,0);

ind_stable = sigma > accuracy*norm(sigma);
num_stable_sv = sum(ind_stable);
prod_stable_sv = prod(sigma(ind_stable));

end