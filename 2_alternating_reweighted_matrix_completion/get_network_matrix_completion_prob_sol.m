function [problem,solution] = get_network_matrix_completion_prob_sol(problem_setting,solution_setting)

%% sample solution
Ytrue = randn(problem_setting.n(1),solution_setting.rXtrue);
Ztrue = randn(solution_setting.rXtrue,problem_setting.n(2));

%% operator L
ell = ceil(min(prod(problem_setting.n),problem_setting.ell_fct(problem_setting.n,solution_setting.rXtrue,sum(problem_setting.n)*solution_setting.rXtrue)));
P = zeros(ell,2);

switch problem_setting.Ltype
    case 'gaussian'
        error('Low rank gaussian matrix operator not (yet) implemented');
        
    case 'sampling'
        k_min = 0;
        while k_min < solution_setting.rXtrue && ~(k_min ~= 0 && min(problem_setting.n) >=50)
            % counted column-wise
            samps = sort(datasample(1:prod(problem_setting.n),ell,'Replace',false));
            % turn to indices
            [P(:,1),P(:,2)] = ind2sub(problem_setting.n,samps(:));
            [dir,dl] = gen_sampling_directories(P,problem_setting.n);
            
            k_min = min(dl(:));
        end
        
        y = zeros(ell,1);
        for i = 1:problem_setting.n(2)
            y(dir(i,1:dl(i,2),2)) = Ytrue(P(dir(i,1:dl(i,2),2),1),:)*Ztrue(:,i);
        end
end

%% transfer
problem = struct;
solution = struct;

problem.n = problem_setting.n;
problem.P = P;
problem.y = y;
problem.ell = ell;

solution.Ntrue.Y = Ytrue;
solution.Ntrue.Z = Ztrue;

end



