function [x,gamma] = vanilla_0_reweighted_vector_recovery(L,y,n,p,nu)
warning('This version is for introduction purposes only. Otherwise, use "reweighted_vector_recovery" instead.');

%% init x and gamma
x = reshape(lsqminnorm(L,y),[n,1]);
gamma = norm(x)^2;
c = 0;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
for iter = 1:1e6
    %% update iterate
    Wm = diag((x.^2 + gamma).^(1-p/2));
    x = Wm*L' * ( (L*Wm*L') \ y );
    
    %% print
    if sqrt(gamma)/norm(x) < 10^(-c)
        fprintf('iter: %d, sqrt(gamma): %.2e, fa/log(gamma): %.2f \n',iter,sqrt(gamma),-1/log(gamma)*sum(log(x.^2/gamma+1)));
        c = c + 1;
    end
    
    %% consider break
    if sqrt(gamma)/norm(x) < 1e-7
        break;
    end
    
    %% decrease gamma
    gamma = nu * gamma;     
end

end



