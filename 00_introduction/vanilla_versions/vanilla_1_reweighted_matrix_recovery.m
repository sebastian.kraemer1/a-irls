function [X,iter] = vanilla_1_reweighted_matrix_recovery(Lbar,y,n,p,nu)
warning('This version is for introduction purposes only. Otherwise, use "reweighted_matrix_recovery" instead.');

%% init X
X = reshape(lsqminnorm(Lbar,y),n);
gamma = norm(X,'fro')^2;
c = 0;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
for iter = 1:1e6
    %% update iterate
    Wm = (X*X' + gamma*eye(n(1)))^(1-p/2);
    Wmbar = kron(eye(n(2)),Wm);
    
    X(:) = Wmbar*Lbar' * ( (Lbar*Wmbar*Lbar') \ y );
    
    %% print
    if sqrt(gamma)/norm(X,'fro') < 10^(-c)
        fprintf('iter: %d, sqrt(gamma): %.2e \n',iter,sqrt(gamma));
        c = c + 1;
    end
    
    %% consider break
    if sqrt(gamma)/norm(X,'fro') < 1e-7
        break;
    end
    
    %% decrease gamma
    gamma = nu * gamma; 
    
end

end



