# A-IRLS Library

Matlab implementations of (alternating) IRLS algorithms for affine vector cardinality, matrix rank and tensor sum-of-rank minimization.
As the code is more ortiented towards practice, the functions are accordingly named vector, matrix and tensor recovery.

--- --- ---

For software copyright issues, see the LICENSE file.

Please note the copyright and citation issues implied by the publication and preprint versions of the related papers
"Asymptotic Log-Det Rank Minimization via (Alternating) Iteratively Reweighted Least Squares" and "Asymptotic Log-Det Sum-of-Ranks Minimization via Tensor (Alternating) Iteratively Reweighted Least Squares" each by Sebastian Kraemer.

--- --- ---

HOW TO GET STARTED:

Open the Matlab live scripts in the folder 00_introduction. These may serve as initial guide.
As further documentation is not yet included, please note the test_ functions. Each serves as introduction to the corresponding method.

For the tensor functions, the following two repositories are required:

The 'tensor node toolbox' (beta version) by Sebastian Kraemer for automatic network contraction and visualization:
https://git.rwth-aachen.de/sebastian.kraemer1/tensor-node-toolbox

'multiprod.m' written by Paolo de Leva (available on mathworks.com) for faster multiplication of arrays of matrices:
https://www.mathworks.com/matlabcentral/fileexchange/8773-multiple-matrix-multiplications-with-array-expansion-enabled



