function [problem,solution] = get_vector_recovery_prob_sol(problem_setting,solution_setting)

%% sample solution 
nonzero = datasample(1:problem_setting.n,solution_setting.rxtrue,'Replace',false);
xtrue = zeros(problem_setting.n,1);
xtrue(nonzero) = randn(numel(nonzero),1);
while true
    ind = abs(xtrue(nonzero)) < solution_setting.min_non_zero_val;
    if ~any(ind)
        break;
    end
    xtrue(nonzero(ind)) = randn(sum(ind),1);
end

%% operator L
ell = ceil(min(problem_setting.n,problem_setting.ell_fct(problem_setting.n,solution_setting.rxtrue)));

switch problem_setting.Ltype
    case 'gaussian'
        L = randn(ell,prod(problem_setting.n));
        L = L/norm(L,'fro');      
end

%% measurements y
y = L*xtrue;

%% transfer
problem = struct;
solution = struct;

problem.n = problem_setting.n;
problem.L = L;
problem.y = y;
problem.ell = ell;

solution.xtrue = xtrue;