function [success,diff,quot,rel_nres_y,rel_nres_x_true] = evaluate_vector(L,x,xtrue,y,accuracy)

success = false;

%% rank and singular values with respect to accuracy
[num_stable_en,prod_stable_en] = eval_this(x,accuracy);
[num_stable_en_true,prod_stable_en_true] = eval_this(xtrue,accuracy);

%% difference and quotient
diff = num_stable_en - num_stable_en_true;
quot = prod_stable_en / prod_stable_en_true;

%% residuals
rel_nres_y = norm(L*x-y)/norm(y);
rel_nres_x_true = norm(x-xtrue)/norm(xtrue);

%% check whether success (1) or improvement (2)
if rel_nres_y < accuracy
    if diff == 0 && quot > 0.98 && quot < 1.005
        success = true;
    end
    if diff < 0 || diff == 0 && quot <= 0.98
        success = 2;
    end
end

end

function [num_stable_en,prod_stable_en] = eval_this(x,accuracy)

ind_stable = abs(x) > accuracy*norm(x);
num_stable_en = sum(ind_stable);
prod_stable_en = prod(abs(x(ind_stable)));

end