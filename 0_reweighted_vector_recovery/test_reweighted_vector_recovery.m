function test_reweighted_vector_recovery()

%% parameters

% problem:
problem_setting.n = 100;
problem_setting.Ltype = 'gaussian';
problem_setting.ell_fct = @(n,r) 40;

% reference solution:
solution_setting.rxtrue = 20;
solution_setting.min_non_zero_val = 1e-3;

% important meta parameters
meta_para.opt_method = 'image';
meta_para.f_gamma = 1.001;

%% get problem
[problem,solution] = get_vector_recovery_prob_sol(problem_setting,solution_setting);



%% run (p = 0)
meta_para.weight_p = 0;
x_alg = reweighted_vector_recovery(problem,solution,meta_para);   

%% evaluate
accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_x_true] = evaluate_vector(problem.L,x_alg,solution.xtrue,problem.y,accuracy);
fprintf('p = 0 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_x_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_x_true);



%% run (p = 1)
meta_para.weight_p = 1;
x_alg = reweighted_vector_recovery(problem,solution,meta_para);   

%% evaluate
accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_x_true] = evaluate_vector(problem.L,x_alg,solution.xtrue,problem.y,accuracy);
fprintf('p = 1 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_x_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_x_true);
