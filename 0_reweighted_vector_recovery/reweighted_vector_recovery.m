function [x,gamma] = reweighted_vector_recovery(problem,solution,meta_para)

close all

    function suggest_default(field,default)
        if ~isfield(meta_para,field)
            meta_para.(field) = default;
        end
    end

%% Parameter defaults (preferences)

% weight parameter p
suggest_default('weight_p',0);
if meta_para.weight_p ~= 0
    warning('not using rank weights (p = 0) but p = %f',meta_para.weight_p);
end

% preferences
suggest_default('iter_max',10000000); % will break earlier by itself
suggest_default('print_interval',1000);
suggest_default('plot_interval',1000);
suggest_default('f_gamma',1.01);
suggest_default('opt_method','image');

% breaking criteria
suggest_default('rel_gamma_min',1e-10);
suggest_default('nres_min',0.8e-6);

suggest_default('rel_s_min',1e-10);
suggest_default('s_gamma_factor',10);

% heuristic, well working choices
suggest_default('omega',0.5);
suggest_default('marginal_factor',1e-12);
suggest_default('gamma_start_factor',10);
suggest_default('image_kernel_switch',1e-6);

%% read problem setting
n = problem.n;
L = problem.L;
y = problem.y;

xtrue = solution.xtrue;

% magnitudes
norm_L = norm(L,'fro');
scaling_L = n/norm_L^2;

%% svd of xtrue
s_true = sort(abs(xtrue),'descend');
ind_stable_true = s_true/norm(xtrue) > meta_para.marginal_factor;
num_stable_sv_true = sum(ind_stable_true);
prod_stable_sv_true = prod(s_true(ind_stable_true));

norm_Xtrue = norm(s_true);
norm_y = norm(y);

%% for method kernel
Kbar = null(L);

%% for method relaxed
omega = meta_para.omega;

%% init x
x = reshape(lsqminnorm(L,y),[n,1]);
x0 = x;

gamma = meta_para.gamma_start_factor*norm(x)^2;
S = [];
Gamma = [];
Res_y = [];

% absolute values
s = sort(abs(x),'descend');

%% colors for plot
blue = [0,83,159]/255;
teal = [0,152,161]/255;
bord = [161,16,53]/255;
viol = [97,33,88]/255;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
tic

opt_method = meta_para.opt_method;

for iter = 1:meta_para.iter_max
    % update iterate
    switch opt_method
        case 'kernel'
            W = diag((x.^2+gamma).^(meta_para.weight_p/2-1));
            
            x(:) = x0(:) - Kbar* ( (Kbar'*W*Kbar) \ (Kbar'*W*x0(:)) );
            
        case 'image'
            Wm = diag((x.^2 + gamma).^(1-meta_para.weight_p/2));
            WLT = Wm*L';
            x(:) = WLT* ( (L*WLT) \ y );
            
        case 'relaxed'
            W = diag((x.^2+gamma).^(meta_para.weight_p/2-1));
            
            scaling_W = omega^2*gamma;
            H = scaling_L*(L'*L) + scaling_W*W;
            u = scaling_L*L'*y;
            x(:) = H \ u;
    end
    
    % absolute values
    s = sort(abs(x),'descend');
    
    % evaluate progress
    ind_stable = s > sqrt(gamma);
    last_stable_sv = s(find(~ind_stable,1));
    
    nres_X_true = norm(x-xtrue);
    nres_y = norm(L*x(:)-y);
    nres_y_scaled = nres_y*sqrt(scaling_L);
    rel_nres_y = nres_y/norm_y;
    
    switch opt_method
        case 'relaxed'
            f_scaled = sqrt(scaling_L*nres_y^2 + scaling_W*(log(prod((s.^2+gamma)/gamma))));
        otherwise
            f_scaled = gamma*(sum(log((s.^2+gamma)/gamma)));
    end
    
    num_stable_sv = sum(ind_stable);
    prod_stable_sv = prod(s(ind_stable));
    
    % print
    print_input = {iter,toc,sqrt(gamma),last_stable_sv/sqrt(gamma),num_stable_sv,num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true/norm_Xtrue};
    if ~mod(iter,meta_para.print_interval)
        print_progress(print_input{:});
    end
    
    % plot
    if ~mod(iter,meta_para.plot_interval)
        S = [S; s(:)']; %#ok<AGROW>
        Gamma = [Gamma; sqrt(gamma)]; %#ok<AGROW>
        
        hold off;
        
        semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Gamma(:),'-','color',teal);
        hold on;
        
        for i = 1:size(S,2)
            ind = S(:,i) > 1e-10*norm(s);
            p1 = meta_para.plot_interval:meta_para.plot_interval:iter;
            p2 = S(:,i);
            semilogy(p1(ind),p2(ind),'-','color',blue);
        end
        for i = 1:num_stable_sv_true
            semilogy([meta_para.plot_interval,iter],[s_true(i),s_true(i)],'--','color',viol);
        end
        
        if strcmp(opt_method,'relaxed')
            Res_y = [Res_y; nres_y_scaled]; %#ok<AGROW>
            semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Res_y(:),':','color',bord);
        end

        drawnow
    end
    
    % consider break
    if sqrt(gamma)/norm(x(:)) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
        print_progress(print_input{:});
        break;
    end
    
    
    % decrease gamma
    gamma = gamma / meta_para.f_gamma;
    
    % change method to avoid ill-conditioning
    if sqrt(gamma)/norm(s) < meta_para.image_kernel_switch && strcmp(opt_method,'image')
        opt_method = 'kernel';
    end
    
    
end

end

function print_progress(iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue)

fprintf('iter: %4d, time: %2.2f, sqrt(gamma): %.2e, quot_gamma_stable: %.2e, num_stable: %d/%d, quot_prod_stable: %.2e, f_sc: %.2e, nres_y_sc %.2e, rel_nres_X_true: %.2e \n',...
    iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue);

end



