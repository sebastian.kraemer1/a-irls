function [success,diff,quot,rel_nres_y,rel_nres_N_true,sigma,sigma_true] = evaluate_network_tensor(L,N,Ntrue,y,accuracy)

success = false;

%% ranks and singular values with respect to accuracy
[num_stable_sv_true,prod_stable_sv_true,sigma_true] = eval_this(Ntrue,accuracy);
[num_stable_sv,prod_stable_sv,sigma] = eval_this(N,accuracy);

%% difference and quotient
diff = num_stable_sv - num_stable_sv_true;
quot = prod_stable_sv / prod_stable_sv_true;

%% residuals
% [~,~,rel_nres_y] = net_dist({L,N},y); may exceed memory capacity if
% unlucky but appr. yields same result (if control necessary)
rel_nres_y = calc_rel_nres_y(L,N,y);
[~,~,rel_nres_N_true] = net_dist(N,Ntrue);

%% check whether success (1) or improvement (2)
if rel_nres_y < accuracy
    if diff == 0 && quot > 0.98 && quot < 1.005
        success = true;
    end
    if diff < 0 || diff == 0 && quot <= 0.98
        success = 2;
    end
end

end

function [num_stable_sv,prod_stable_sv,sigma_unfold] = eval_this(N,accuracy)

[~,~,sigma] = STAND(N,1);
sigma_unfold = cell(1,numel(sigma));

norm_N = net_norm(N);
num_stable_sv = 0;
prod_stable_sv = 1;
for i = 1:length(sigma)
    sigma_unfold{i} = sigma{i}.data;
    ind_stable = sigma_unfold{i} > accuracy*norm_N;
    num_stable_sv = num_stable_sv + sum(ind_stable);
    prod_stable_sv = prod_stable_sv*prod(sigma_unfold{i}(ind_stable));
end

end


function rel_nres_y = calc_rel_nres_y(L,N,y)

G = net_derive_G(N);
V = 1:numel(N);
nV = length(V);
root = 1;
zeta = 'zeta';

branchLN = cell(nV);
for p = V   
    branchLN{p} = cell(1,nV);
end

INIT_REC_LTR(root,[]);
Neigh = neighbors(G,root)';
LN = boxtimes(branchLN{root}{Neigh},L{root},N{root},'^',zeta,'mode','optimal');
[~,~,rel_nres_y] = net_dist(LN,y);

    function INIT_REC_LTR(b,P)
        % recursively initializes evaluation and remembers inner labels     
        Neighb = neighbors(G,b)';
        
        for h = setdiff(Neighb,P)
            INIT_REC_LTR(h,b);
        end
        
        if ~isempty(P)
            
            H_ = int_setdiff(Neighb,P);
            branchLN{P}{b} = boxtimes(branchLN{b}{H_},L{b},N{b},'^',zeta);
            
        end
    end

end

