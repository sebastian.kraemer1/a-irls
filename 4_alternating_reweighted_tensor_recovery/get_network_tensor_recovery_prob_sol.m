function [problem,solution] = get_network_tensor_recovery_prob_sol(problem_setting,solution_setting)

if ~isfield(solution_setting,'min_sing_val')
    solution_setting.min_sing_val = 1e-3;
end

n_ = problem_setting.n_values;
d = numel(n_);
alpha = mna('alpha',1:d);

n = assign_mode_size(alpha,n_);

cardK = length(problem_setting.HTmathcalK);
alphaK = cell(1,cardK);
for i = 1:cardK
    alphaK{i} = alpha(problem_setting.HTmathcalK{i});
end

cardK = length(solution_setting.HTmathcalKtrue);
alphaKtrue = cell(1,cardK);
for i = 1:cardK
    alphaKtrue{i} = alpha(solution_setting.HTmathcalKtrue{i});
end

%% network Ntrue
n_true = n;
[Gtrue,mNtrue,~,~,beta] = RTLGRAPH(alpha,alphaKtrue,'beta');
for mn = beta
    n_true.(mn{1}) = solution_setting.rNtrue; % 1+randi(solution_setting.rNtrue-1); % 
end
Ntrue = init_net(mNtrue,n_true);
Ntrue = randomize_net(Ntrue);
leNtrue = length(Ntrue);

%% exponentially decaying values?
decay_factor = solution_setting.exp_decay_sv;

if solution_setting.exp_decay_sv
    % this is not suitable to reliably assign specific singular values
    [~,fNtrue,sigma_true,sigma_map] = STAND(Ntrue,1);
    for i = 1:length(sigma_true)
        % careful: this trusts on fitting Ntrue, sigma_map and fNtrue
        while true
            values = (decay_factor).^(randn(length(sigma_true{i}.data),1));
            values = values / norm(values);
            if ~any(values < solution_setting.min_sing_val)
                break;
            end
        end
            
        sigma_true{i}.data = values;
        beta_i = sigma_true{i}.mode_names{1};
        v = Gtrue.Edges.EndNodes(sigma_map.(beta_i),2);
        fNtrue{v} = boxtimes(fNtrue{v},sigma_true{i},'^',beta_i);
    end
    Ntrue = fNtrue;
end

%% normalize Ntrue
norm_Ntrue = net_norm(Ntrue);
for i = 1:leNtrue
    Ntrue{i}.data = Ntrue{i}.data / norm_Ntrue^(1/length(Ntrue));
end

ell = min(prod(n_),ceil(problem_setting.ell_fct(n_,solution_setting.rNtrue,data_volume(Ntrue))));
fprintf('Total measurement ratio of %.2e%%.\n',ell/prod(n_)*100);

%% network L
[~,mL,~,~,epsilon] = RTLGRAPH(alpha,alphaK,'epsilon');
if strcmp(problem_setting.Ltype,'sampling')
    if isfield(problem_setting,'rL') && problem_setting.rL ~= 1
        error('Ranks of sampling operators equal 1 (remove problem_setting.rL)');
    else
        problem_setting.rL = 1;
    end
end
n_epsilon = assign_mode_size(epsilon,problem_setting.rL);
n = merge_fields(n,n_epsilon);

zeta = 'zeta';
n.(zeta) = ell;
alpha_location = zeros(1,d);

for i = 1:length(mL)
    alpha_pos = ismember(alpha,mL{i});
    if any(alpha_pos)
        alpha_location(alpha_pos) = i;
        mL{i} = union(mL{i},'zeta');
    end
end

L = init_net(mL,n);

switch problem_setting.Ltype
    case 'gaussian'
        L = randomize_net(L);
        L = [L,'^',zeta];
        norm_L = net_norm(L);
        for i = 1:length(L)-2
            L{i}.data = L{i}.data / norm_L^(1/(length(L)-2));
        end
    case 'sampling'
        for i = 1:length(L)
            L{i}.data(:) = 1;
        end
        
        sampling_d = [];
        trials = 0;
        num_dubl = ell;
        while num_dubl > 0
            sampling_add = zeros(num_dubl,d);
            for i = 1:d
                sampling_add(:,i) = randi(n_(i),num_dubl,1);
            end
            sampling_d = unique([sampling_d;sampling_add],'rows');
            num_dubl = ell - size(sampling_d,1);
            
            trials = trials + 1;
            if trials == 100
                error('No unique sampling found');
            end
        end

        for i = 1:d
            I = eye(n_(i)); 
            temp = {zeta,alpha{i}};
            rem_mn = str_setdiff(mL{alpha_location(i)},temp);
            L{alpha_location(i)} = fold(I(sampling_d(:,i),:),[zeta,alpha{i},rem_mn],n);
        end
        L = [L,'^',zeta];
end

y = boxtimes(L,Ntrue);
% y.data = y.data + randn(size(y.data))*norm(y.data(:))/100;



%% transfer
%% transfer
problem = struct;
solution = struct;

problem.n = n;
problem.d = d;
problem.alpha = alpha;
problem.L = L;
problem.y = y;
problem.zeta = zeta;
problem.alphaK = alphaK;
problem.B = [];
problem.lambda = 0;
problem.ell = ell;

solution.Ntrue = Ntrue;
solution.alphaKtrue = alphaKtrue;


