function [N,info] = alternating_reweighted_tensor_recovery(problem,validation,solution,meta_para)
% weight_p; p = 0: rank weights (use this), p = 1: nuclear norm weights
% ---
% optimization methods:
% full: use path rec instead
% path: use path rec instead
% path_rec: recursive evaluation, never using cg 
% path_rec_cg: will determine if cg might be preferrable (use this)
% neigh: use for easier problems (corresponds to max_path_length = 2)
% none: use for very easy problems
% ---

close all
activate_boxtimes_mem(100000)

    function suggest_default(field,default)
        if ~isfield(meta_para,field)
            meta_para.(field) = default;
        end
    end

%% Parameter defaults (preferences)
% weight parameter p
suggest_default('weight_p',0);
if meta_para.weight_p ~= 0
    warning('not using rank weights (p = 0) but p = %f',meta_para.weight_p);
end
% print every such iteration
suggest_default('print_interval',10);
% plot every such iteration
suggest_default('plot_interval',10);
% do or do not record movie
suggest_default('record_movie',false); % (movie2gif can compile files to gifs)

% rate of decline of gamma (aka nu)
suggest_default('f_gamma',1.02);
% maximal rank of iterate
suggest_default('rN_uni_max',8);
% optimization method (see above)
suggest_default('opt_method','path_rec_cg');
% maximal path length for weight matrix
suggest_default('max_path_length',Inf); % (2 = only neighbors, Inf = no limit)
% do or do not explicitelz adapt the rank
suggest_default('adapt_rank_expl',true);
% if not, change (starting) ranks
suggest_default('rN_uni_start',3);

%% Parameter warnings
if strcmp(meta_para.opt_method,'path') || strcmp(meta_para.opt_method,'full')
    warning('use faster mode ''path_rec'' instead')
end
if strcmp(meta_para.opt_method,'none')
    warning('disabeling explicit rank adaption / using maximal ranks');
    meta_para.adapt_rank_expl = false;
    meta_para.rN_uni_start = meta_para.rN_uni_max;
    meta_para.f_gamma = [];
end 

if meta_para.max_path_length~=Inf
    warning('using restrictive path length');
end

if meta_para.max_path_length==2 && ~strcmp(meta_para.opt_method,'path_rec_cg')
    warning('setting optimization mode as ''neigh''');
end

if ~isequal(solution.alphaKtrue,problem.alphaK)
        warning('Sample solution network is different from iterate network. This might not be intended. Plotting might throw errors.');   
end

%% Heuristic, well working tuning (there should be no need to change such)
% misc
suggest_default('iter_min',20);
suggest_default('iter_max',100000); % (algorithm will break earlier by itself)
suggest_default('root_start',1);
suggest_default('gamma_start_factor',10);
suggest_default('omega',0.5);

% explicit rank adaption
suggest_default('num_minor_sv',2);
suggest_default('rank_increase_count_threshold',4);
suggest_default('rank_decrease_count_threshold',20);
suggest_default('new_minor_sv_factor',1/100);
suggest_default('minor_sv_function',@(s,gamma) s < sqrt(gamma));
suggest_default('pseudo_gamma_factor',1/10);
suggest_default('last_gamma_factor',1/200);

% breaking criteria
suggest_default('nres_res_max_factor',2);
suggest_default('gamma_nres_factor',1e-2);
suggest_default('nres_min',0.8e-6);
suggest_default('rel_gamma_min',5e-8); % 1e-7
suggest_default('post_iterate',true);
suggest_default('post_iteration_progress_factor',1.001);
suggest_default('use_validation_breaking_critera',true);
suggest_default('opt_none_stag_factor',5e-6); % 1e-5

% conjugate gradient solver (for path_rec_cg mode)
suggest_default('cg_break_fac',0.001);
suggest_default('absolute_cg_break_tol',1e-8);
suggest_default('cg_max_volume',1200);

meta_para_orig = meta_para;

%% read problem setting
n = problem.n; 
L = problem.L;
Lval = validation.Lval;
y = problem.y;
yval = validation.yval;

alphaK = problem.alphaK;
alpha = problem.alpha;
zeta = problem.zeta;

Ntrue = solution.Ntrue;

% magnitudes and parameters
prod_n = prod(cell2mat(getfields(n,alpha)));
norm_L = net_norm(L);
norm_y = net_norm(y);

gamma = meta_para.gamma_start_factor*norm_y^2*prod_n/norm_L^2;
Gamma = zeros(meta_para.iter_max,1);
omega = meta_para.omega;

scaling_L = prod_n/norm_L^2;
scaling_W = []; % set before optimization step

[f_scaled,nres_N_true,nres_y,nres_y0] = deal([]);
cardK = length(alphaK);

%% network N / graph properties / empty branch LN evaluation
[G,mN,~,~,beta] = RTLGRAPH(alpha,alphaK,'beta');
V = 1:length(mN);
nV = length(V);
root = meta_para.root_start;

n_beta = assign_mode_size(beta,meta_para.rN_uni_start);
n = merge_fields(n,n_beta); n_last = n;

branch = cell(1,length(mN));
for w = 1:length(mN)
    branch{w} = branches(G,w);
end

alt_order = set_alt_order();

path = cell(nV,nV);
branchLN = cell(nV);

for p = V
    for w = setdiff(V,p)
        path{p,w} = shortestpath(G,p,w);
    end
    
    branchLN{p} = cell(1,nV);
end

mn_inter = cell(nV,nV);

%% Ntrue
[~,~,sigma_true,sigma_map_true] = STAND(Ntrue,1);
norm_Ntrue = net_norm(Ntrue);
num_stable_sv_true = 0;
prod_stable_sv_true = 1;
for i_ = 1:cardK
    sigma_true_unfold_i = sigma_true{sigma_map_true.(beta{i_})}.data;
    ind_stable = sigma_true_unfold_i > 1e-12;
    num_stable_sv_true = num_stable_sv_true + sum(ind_stable);
    prod_stable_sv_true = prod_stable_sv_true*prod(sigma_true_unfold_i(ind_stable));
end

%% validation process
if ~isempty(Lval)
    branchLvalN = cell(nV);
    
    for p = V
        branchLvalN{p} = cell(1,nV);
    end
    
    N_opt = [];
    n_opt = [];
    root_opt = [];
    gamma_opt = 0;
    nres_yval = [];
    nres_yval_min = Inf;
    f_scaled_val_min = Inf;
    nres_yval0 = [];
    
    scaling_Lval = prod_n/net_norm(Lval)^2;
    new_opt = false;
    iter_opt = 0;
    f_scaled_val = [];
end

%% print
% Symbols for ranks over 9...! (just for display)
ALP = cell(1,9+2*24);
for ii = 1:10 % 1 - 9
    ALP{ii} = char(48+ii-1);
end
for ii = 1:24 % > 9
    ALP{10+ii} = char(64+ii);
    ALP{10+24+ii} = char(96+ii);
end
[num_stable_sv,prod_stable_sv] = deal([]);

%% plot
maxsig = 0;
minsig = Inf;

iter_movie = 0;
if meta_para.record_movie
    MOVIE = struct('cdata',[],'colormap',[]);
end

num_sv_true = 0;
for j_ = 1:cardK
    num_sv_true = num_sv_true + length(sigma_true{j_}.data);
end
violet_sings = zeros(num_sv_true,3); num_violet = 0;
for j_ = 1:cardK
    sigma_true_unfold_i = sigma_true{sigma_map_true.(beta{j_})}.data;
    for k_ = 1:length(sigma_true_unfold_i)        
        num_violet = num_violet + 1;
        violet_sings(num_violet,:) = [j_,sigma_true_unfold_i(k_),3];
    end
end

%% nested solver variables
[Q,R,W,NWvN] = deal(cell(1,nV));
NWN = [];

%% path eval
[Path_eval,path_ZZT,W_leaf,branchW,branchpart,branchW_Delta] = deal(cell(1,nV));

%% explicit rank adaption
[minor_sing_val_count,label_not_yet_touched,r_stab] = deal(struct);
for mu = 1:length(beta)
    minor_sing_val_count.(beta{mu}) = 0;
    r_stab.(beta{mu}) = [];
end

%% post iteration
iteration_status = 'main';
[f_scaled0,f_scaled_val0] = deal([]);

%% B components (for nonempty B)
lambda = problem.lambda;
B = problem.B;

% priming 
beta_pr = cell(1,cardK);
[prime_rn,prime_rn_inv] = deal(struct);

for mu = 1:cardK
    beta_pr{mu} = [beta{mu},'_pr'];
    n.(beta_pr{mu}) = n.(beta{mu});
    prime_rn.(beta{mu}) = beta_pr{mu};
    prime_rn_inv.(beta_pr{mu}) = (beta{mu});
end

branchNBN_pr = cell(nV);
for p = V
    branchNBN_pr{p} = cell(1,nV);
end

scaling_B = lambda^2;

%% cg solve
INT_cg = 0;
INT_cg_count = 0;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
tic
[N,sigma,sigma_map,norm_N] = initialize_N();
N_end = [];
INIT_REC_LTR(root,[]);

for iter = 1:meta_para.iter_max
    
    % used for explicit rank adaption
    for mu = 1:length(beta)
        label_not_yet_touched.(beta{mu}) = true;
    end
    
    for c_new = alt_order
        % establishes orthogonality conditions
        move_to(c_new);
        
        % assumes orthogonality conditions
        scaling_W = 1/cardK*omega^2*gamma;
        update_iterate();
        
        % assumes orthogonality conditions
        update_neigh_sing_vals();
    end
    
    decrease_gamma(); 
    evaluate_progress();
    
    if ~mod(iter,meta_para.print_interval)
        print();
    end
    
    if ~mod(iter,meta_para.plot_interval)
        plot_sing_spec()
    end
    
    determine_iteration_status();
    if strcmp(iteration_status,'break')
        break;
    end
    
end

%% final result
fprintf('Final result: \n');
evaluate_progress();
print();
if meta_para.plot_interval ~= 0
    plot_sing_spec();
    figure
    net_view(N)
end
meta_para = meta_para_orig;

if meta_para.plot_interval ~= 0 && meta_para.record_movie && exist('movie2gif') %#ok<EXIST>
    movie2gif(MOVIE, 'movie.gif', 'LoopCount', 5, 'DelayTime', 0.05);
end

info = struct;
info.gamma = gamma;
info.sigma = sigma;
info.N_end = N_end;
info.meta_para = meta_para;
info.n = n;
info.time = toc;
info.iter = iter;

%% essential subfunctions
    function alt_order = set_alt_order()
        % determines sweep order through network N     
        alt_order = zeros(1,nV);
        cur = 1;
        
        ALTERORDERREC(root,[]);
        
        function ALTERORDERREC(b,P)
            Neighb = neighbors(G,b)';
            
            for h = setdiff(Neighb,P)
                ALTERORDERREC(h,b);
            end
            
            alt_order(cur) = b; cur = cur + 1;
        end
        
    end

    function [N,sigma,sigma_map,norm_N] = initialize_N()
        % initializes iterate N (close to rank 1)     
        N = init_net(mN,n);
        N = randomize_net(N);
        
        for v = 1:length(N)
            alpha_of_w = intersect(mN{v},alpha);
            beta_of_w = intersect(mN{v},beta);
            if isempty(alpha_of_w)
                N{v}.data(1) = 1;
            else
                N{v} = set_node_part(N{v},ones(n.(alpha_of_w{1}),1),beta_of_w,num2cell(ones(1,length(beta_of_w))));
            end
        end
        
        [N,~,sigma,sigma_map] = STAND(N,root);
        norm_N = norm(N{root}.data(:));
    end

    function INIT_REC_LTR(b,P)
        % recursively initializes evaluation and remembers inner labels     
        Neighb = neighbors(G,b)';
        
        for h = setdiff(Neighb,P)
            INIT_REC_LTR(h,b);
        end
        
        if ~isempty(P)
            
            H_ = int_setdiff(Neighb,P);
            branchLN{P}{b} = boxtimes(branchLN{b}{H_},L{b},N{b},'^',zeta);
            if ~isempty(B)
                branchNBN_pr{P}{b} = boxtimes(derive_net(prime_rn,N{b}),branchNBN_pr{b}{H_},B{b},N{b},'mode','optimal');
            end
            if ~isempty(Lval)
                branchLvalN{P}{b} = boxtimes(branchLvalN{b}{H_},Lval{b},N{b},'^',zeta);
            end
            mn_inter_Pb_cell = str_intersect(mN{P},mN{b});
            mn_inter{P,b} = mn_inter_Pb_cell{1};
            mn_inter{b,P} = mn_inter_Pb_cell{1};
            
        end
    end

    function update_iterate()   
        % main update to iterate     
        if data_volume(N{root}) < meta_para.cg_max_volume || ~strcmp(meta_para.opt_method,'path_rec_cg')
            get_NWN();
            solve_via_full_components();
        else
            get_NWN_components();
            path_rec_cg_solver();
        end
    end

    function get_NWN()
        % calculates reweighted part of linear subproblem
        switch meta_para.opt_method
            case 'full'
                N_neq_c = N(setdiff(V,root));
                
                beta_of_c = intersect(mN{root},beta);
                alpha_of_c = intersect(mN{root},alpha);
                NWN = init_node([beta_of_c,beta_of_c],n);
                
                for v = setdiff(V,root) %
                    % only use path not longer than para.max_path_length
                    if length(path{root,v}) > meta_para.max_path_length
                        continue;
                    end
                    
                    Q{v} = N(branch{root}{v}{2});
                    R{v} = N(branch{root}{v}{1});
                    ZZT = boxtimes(R{v},R{v},'_',alpha);
                        
                    W{v} = {Q{v},regularized_matrix_p_inverse(ZZT),Q{v},'_\',alpha};
                    
                    NWvN{v} = boxtimes(N_neq_c,W{v},N_neq_c,'_\',mN{root});
                    NWN = add_scale(NWN,NWvN{v});
                end
                
                if ~isempty(alpha_of_c)
                    NWN = boxtimes(NWN,fold(eye(n.(alpha_of_c{1})),{alpha_of_c{1},alpha_of_c{1}},n));
                end
                
            case 'path'
                V_minus_c = setdiff(V,root);
                path_ind = true(1,length(V_minus_c));
                
                for i = 1:length(V_minus_c)
                    v = V_minus_c(i);
                    
                    % only use path not longer than para.max_path_length
                    if length(path{root,v}) > meta_para.max_path_length
                        path_ind(i) = false;
                        continue;
                    end
                    
                    beta_to_v_cell = mn_inter(path{root,v}(end-1),v); % = str_intersect(mN{path{root,v}(end-1)},mN{v});
                    [beta_from_c,other_mn_of_c] = str_intersect(mN{root},mN{path{root,v}(2)});
                    
                    if length(path{root,v})>2
                        Path_eval{v} = boxtimes(N(path{root,v}(2:end-1)),N(path{root,v}(2:end-1)),'_\',[beta_from_c,beta_to_v_cell]);
                        ZZT = boxtimes(N{root},Path_eval{v},N{root});
                    else
                        Path_eval{v} = init_node({},n);
                        ZZT = boxtimes(N{root},N{root},'_\',beta_to_v_cell);
                    end
                    
                    W_ast = regularized_matrix_p_inverse(ZZT); 
                    
                    delta_other = eye_node(other_mn_of_c,n);
                    
                    if length(path{root,v})>2
                        priming = struct;
                        priming.(beta_to_v_cell{1}) = 'beta_to_v_prime';
                        [W_ast_prime,Pv_prime] = prime_node(priming,W_ast,Path_eval{v});
                        
                        NWvN{v} = boxtimes(W_ast_prime,Pv_prime,delta_other,'_',[beta_to_v_cell,'beta_to_v_prime']);
                    else
                        NWvN{v} = boxtimes(W_ast,delta_other);
                    end
                    
                end
                
                NWN = add_scale(NWvN{V_minus_c(path_ind)});
                
            case {'path_rec','path_rec_cg'}
                get_NWN_components();
                
                % multiply with remaining deltas
                Neigh = neighbors(G,root)';
                for v = Neigh
                    [~,other_mn_of_c] = str_intersect(mN{root},mN{v});
                    delta_other = eye_node(other_mn_of_c,n);
                    
                    branchW_Delta{v} = boxtimes(branchW{v},delta_other);
                end
                
                % sum up to regularizer matrix
                NWN = add_scale(branchW_Delta{Neigh});
                
            case 'neigh'
                Neigh = neighbors(G,root)';
                for v = Neigh
                    [beta_to_v_cell,other_mn_of_c] = str_intersect(mN{root},mN{v});
                    
                    ZZT = boxtimes(N{root},N{root},'_\',beta_to_v_cell);    
                    W_ast = regularized_matrix_p_inverse(ZZT); 
                    
                    delta_other = eye_node(other_mn_of_c,n);
                    NWvN{v} = boxtimes(W_ast,delta_other);
                end
                
                NWN = add_scale(NWvN{Neigh});
                
            case 'none'
                NWN = init_node([mN{root},mN{root}],n);
        end
    end

    function solve_via_full_components()
        % solves linear subproblem directly
        Neigh = neighbors(G,root)';
        
        LNTLN_half = boxtimes(branchLN{root}{Neigh},L{root},'^',zeta,'mode','optimal');
        LNTLN = boxtimes(LNTLN_half,LNTLN_half,'_',zeta,'mode','optimal');
        
        if ~isempty(B)
            NBN_pr = boxtimes(branchNBN_pr{s}{Neigh},B{s},'mode',contraction_mode);
            NBN = derive_net(prime_rn_inv,NBN_pr);
            
            H = add_scale(scaling_L,LNTLN,scaling_W,NWN,scaling_B,NBN);
        else
            H = add_scale(scaling_L,LNTLN,scaling_W,NWN);
        end
        
        u = add_scale(scaling_L,boxtimes(LNTLN_half,y));
        N{root} = node_linsolve(H,u);
    end

    function get_NWN_components()
        % gets components for weight part in path evaluation method
        [path_ZZT,W_leaf,branchW,branchpart] = deal(cell(1,nV));
        path_ZZT{root} = fold(1,{},n);
        
        % evaluate ZZT matrices from root to leaves
        PATH_ZZT_REC_RTL(root,[],1)
        
        % invert WJ matrices
        V_minus_c = setdiff(V,root);
        for v = V_minus_c
            if length(path{root,v}) > meta_para.max_path_length
                continue;
            end  
            W_leaf{v} = regularized_matrix_p_inverse(path_ZZT{v});
        end
        
        % evaluate and sum up branchW matrices
        BRANCHW_REC_LTR(root,[],1);   
    end

    function result = regularized_matrix_p_inverse(matrix_node)
        % calculate (A + gamma I)^(-(1-p/2))
        [Q_,D_] = eig(matrix_node.data);
        result = fold(Q_*diag((diag(D_)+gamma).^(-(1-meta_para.weight_p/2)))*Q_',matrix_node.mode_names,n);  
    end

    function move_to(h)
        % path to new node in tree graph
        path_to_h = path{root,h}; %shortestpath(m_graph,root,h);
        
        for path_index = 1:length(path_to_h)-1
            s_old = path_to_h(path_index);
            s_new = path_to_h(path_index+1);
            
            % svd in forward direction
            mn_inter_s = mn_inter{s_old,s_new}; % = str_intersect(mN{s_old},mN{s_new});
            sigma_ind = sigma_map.(mn_inter_s);
            
            [U,sigma{sigma_ind},Vt,n.(mn_inter_s)] = node_svd(N{s_old},listdiff(N{s_old}.mode_names,mn_inter_s),mn_inter_s,-1);
            
            N{s_old} = U;
            N{s_new} = boxtimes(Vt,N{s_new});
            
            if meta_para.adapt_rank_expl
                if rank_of_edge_shall_be_increased(s_old,s_new)
                    increase_rank_of_edge(s_old,s_new);
                    minor_sing_val_count.(mn_inter_s) = 0;
                elseif rank_of_edge_shall_be_decreased(s_old,s_new)
                    decrease_rank_of_edge(s_old,s_new);
                    minor_sing_val_count.(mn_inter_s) = 0;
                end
            end
            
            N{s_new} = boxtimes(sigma{sigma_ind},N{s_new},'^',mn_inter_s);
            
            % carry on branch computations
            neigh_wo_new = int_setdiff(neighbors(G,s_old),s_new);
            
            if ~isempty(branchLN{s_old}) || isempty(neigh_wo_new)
                LN = boxtimes(L{s_old},N{s_old});
                branchLN{s_new}{s_old} = boxtimes(branchLN{s_old}{neigh_wo_new},LN,'^',zeta,'mode','optimal');
                
                if ~isempty(B)
                    branchNBN_pr{s_new}{s_old} = boxtimes(derive_net(prime_rn,N{s_old}),branchNBN_pr{s_old}{neigh_wo_new},B{s_old},N{s_old},'mode','optimal');
                end
                if ~isempty(Lval)
                    LvalN = boxtimes(Lval{s_old},N{s_old});
                    branchLvalN{s_new}{s_old} = boxtimes(branchLvalN{s_old}{neigh_wo_new},LvalN,'^',zeta,'mode','optimal');
                end
            end
            
            % formally finish single step by updating root
            root = s_new;
        end
        
    end

    function update_neigh_sing_vals() 
        % updates s.v. neighboring to current root and checks for minor ones
        Neigh = neighbors(G,root)';
        
        for h = Neigh
            mn_inter_s = mn_inter{root,h};
            sigma_ind = sigma_map.(mn_inter_s);
            [~,sigma{sigma_ind},~] = node_svd(N{root},str_setdiff(N{root}.mode_names,{mn_inter_s}),mn_inter_s,0);
            
            if meta_para.adapt_rank_expl
                sigma_unf = unfold(sigma{sigma_ind});
                minor_sv = is_minor_sv(sigma_unf);
                
                if sum(minor_sv) < meta_para.num_minor_sv && label_not_yet_touched.(mn_inter_s)
                    minor_sing_val_count.(mn_inter_s) = max(0,minor_sing_val_count.(mn_inter_s) + 1);
                end
                if sum(minor_sv) > meta_para.num_minor_sv && label_not_yet_touched.(mn_inter_s)
                    minor_sing_val_count.(mn_inter_s) = min(0,minor_sing_val_count.(mn_inter_s) - 1);
                end
                
                label_not_yet_touched.(mn_inter_s) = false;
                r_stab.(mn_inter_s) = n.(mn_inter_s) - sum(minor_sv);
                if sum(minor_sv) && strcmp(iteration_status,'consider_post_iteration_break') && ~numel(sigma_unf) == 1 + meta_para.num_minor_sv
                    iteration_status = 'continue_post_iteration';
                end
            end
        end
        
        norm_N = norm(N{root}.data(:));
    end

    function decrease_gamma()
        % decreases gamma by a constant factor
        if iter > 1
            if strcmp(iteration_status,'main')
                if ~strcmp(meta_para.opt_method,'none')
                    gamma = gamma / meta_para.f_gamma;
                else
                    gamma = scaling_L*nres_y^2;
                end
                
                if ~isempty(Lval)
                    gamma = min(gamma,gamma_opt);
                end
            end
        end
        Gamma(iter) = gamma;
    end

    function do_break = is_to_terminate()
        do_break = true;
         
        if iter > meta_para.iter_min                     
            if ~isempty(Lval) && meta_para.use_validation_breaking_critera
                % divergence
                if nres_yval > nres_yval_min*meta_para.nres_res_max_factor
                    fprintf('Validation suggests to terminate. \n');
                    return;
                end
                
                % divergence
                nres_yval_scaled = nres_yval*sqrt(scaling_Lval);
                if sqrt(gamma) < meta_para.gamma_nres_factor*nres_yval_scaled
                    fprintf('Parameter gamma much lower than validation residual. \n');
                    return
                end

                if strcmp(meta_para.opt_method,'none')
                    % stagnation
                    if abs(nres_yval-nres_yval0)/nres_yval0 < meta_para.opt_none_stag_factor
                        fprintf('Too slow validation improvement. \n');
                        return
                    end
                end
            end
                
            % stagnation
            nres_y_scaled = nres_y*sqrt(scaling_L);
            if sqrt(gamma) < meta_para.gamma_nres_factor*nres_y_scaled
                fprintf('Parameter gamma much lower than sample residual. \n');
                return
            end
            
            if strcmp(meta_para.opt_method,'none')
                % stagnation
                if abs(nres_y-nres_y0)/nres_y0 < meta_para.opt_none_stag_factor
                    fprintf('Too slow improvement. \n');
                    return
                end
            end
        end
        
        % convergence
        if nres_N_true/norm_Ntrue < meta_para.nres_min
            fprintf('Converged to sample solution. \n');
            return;
        end
        
        % parameter or residual convergence
        if sqrt(gamma)/norm_N < meta_para.rel_gamma_min || strcmp(meta_para.opt_method,'none') && nres_y/norm_y < meta_para.nres_min
            fprintf('Residual or gamma converged close to zero. \n');
            return;
        end

        do_break = false;        
    end

    function determine_iteration_status()
        switch iteration_status
            case 'main'
                if is_to_terminate()
                    N_end = N;
                    
                    if ~meta_para.post_iterate
                        iteration_status = 'break';
                    	return;
                    end
                    
                    meta_para.num_minor_sv = 0;
                    meta_para.rank_decrease_count_threshold = 1;
                    iteration_status = 'consider_post_iteration_break'; 
                    fprintf('Beginning post iteration.\n');
                   
                    if ~isempty(Lval) && meta_para.use_validation_breaking_critera
                        N = N_opt;
                        n = n_opt;
                        root = root_opt;
                        gamma = gamma_opt;
                        INIT_REC_LTR(root,[]);
                        
                        fprintf('Returning to iteration number %d using adapted gamma.\n',iter_opt);
                    else
                        min_stab = Inf;
                        for i = 1:cardK
                           sigma_i_unfold = sigma{i}.data;
                           min_stab = min(min_stab,min(sigma_i_unfold(~is_minor_sv(sigma_i_unfold))));
                        end
                        gamma = meta_para.last_gamma_factor*min_stab^2;
                    end
                end
                
            case 'continue_post_iteration'
                % will enter this if rank was truncated
                iteration_status = 'consider_post_iteration_break';
                
            case 'consider_post_iteration_break'
                if ~isempty(Lval)
                    if f_scaled_val > f_scaled_val0/meta_para.post_iteration_progress_factor
                        iteration_status = 'break';
                    end
                else
                    if f_scaled > f_scaled0/meta_para.post_iteration_progress_factor
                        iteration_status = 'break';
                    end
                end
        end
    end

%% recursive path evaluation
    function PATH_ZZT_REC_RTL(b,P,path_length)
        % evaluate ZZT matrices from root to leaves
        if path_length + 1 > meta_para.max_path_length
            return;
        end
        
        Neighb = neighbors(G,b)';
        
        for h = int_setdiff(Neighb,P)
            mn_inter_hb = mn_inter{h,b}; % = str_intersect(mN{h},mN{b});
            
            path_ZZT{h} = boxtimes(N{b},path_ZZT{b},N{b},'_\',mn_inter_hb,'mode','optimal');
            
            PATH_ZZT_REC_RTL(h,b,path_length + 1);
        end
    end

    function BRANCHW_REC_LTR(b,P,path_length)
        % evaluate and sum up branchW matrices
        Neighb = neighbors(G,b)';
        
        if path_length + 1 > meta_para.max_path_length
            H_ = [];
        else
            H_ = int_setdiff(Neighb,P);
        end
        
        for h = H_
            BRANCHW_REC_LTR(h,b,path_length + 1);
        end
        
        if ~isempty(P)
            
            for h = H_
                mn_inter_Pb = mn_inter{P,b}; % = str_intersect(mN{P},mN{b});
                branchpart{h} = boxtimes(N{b},branchW{h},N{b},'_\',mn_inter_Pb,'mode','optimal');
            end
            
            branchW{b} = add_scale(branchpart{H_},W_leaf{b});
        end
    end

%% evaluate and document
    function evaluate_progress()
        % singular values will be approximately up to date
        % [~,~,sigma] = STAND(N,root); % uncomment for exact singular values
            
        if ~isempty(Lval)
            f_scaled_val0 = f_scaled_val;
        end
        f_scaled0 = f_scaled;
         
        log_sum = 0;
        sumnJ = 0;
        sigma_unfold = cell(1,cardK);
        num_stable_sv = 0;
        prod_stable_sv = 1;
        for i = 1:length(sigma)
            sigma_unfold{i} = unfold(sigma{i});
            log_sum = log_sum + sum(log(sigma_unfold{i}.^2+gamma));
            ind_stable = ~is_minor_sv(sigma_unfold{i});
            num_stable_sv = num_stable_sv + sum(ind_stable);
            prod_stable_sv = prod_stable_sv*prod(sigma_unfold{i}(ind_stable));
            sumnJ = sumnJ + length(sigma_unfold{i});
        end
        
        Neigh = neighbors(G,root)';
        
        LN = boxtimes(branchLN{root}{Neigh},L{root},N{root},'^',zeta,'mode','optimal');
        nres_y0 = nres_y;
        nres_y = net_dist(LN,y);
        f_scaled = sqrt(scaling_L*nres_y^2 + scaling_W*(log_sum - sumnJ*log(gamma)));
        
        nres_yval = [];
        if ~isempty(Lval)
            LvalN = boxtimes(branchLvalN{root}{Neigh},Lval{root},N{root},'^',zeta,'mode','optimal');
            nres_yval0 = nres_yval;
            nres_yval = net_dist(LvalN,yval);
            nres_yval_scaled = nres_yval*sqrt(scaling_Lval);
            
            gamma_pseudo = meta_para.pseudo_gamma_factor*nres_yval_scaled^2;
            
            log_sum1 = 0;
            for i = 1:length(sigma)
                log_sum1 = log_sum1 + sum(log(sigma_unfold{i}.^2+gamma_pseudo^2));
            end

            f_scaled_val = sqrt(scaling_Lval*nres_yval^2 + 1/cardK*omega^2*gamma_pseudo^2*(log_sum1 - sumnJ*log(gamma_pseudo^2)));            
            if f_scaled_val < f_scaled_val_min
                N_opt = N;
                n_opt = n;
                root_opt = root;
                gamma_opt = gamma_pseudo;
                nres_yval_min = nres_yval;
                f_scaled_val_min = f_scaled_val;
                new_opt = true;
                iter_opt = iter;
            else
                new_opt = false;
            end
        end

        nres_N_true = net_dist(N,Ntrue);        
    end

    function print()
        % print progress
        fprintf('r_stab:');
        for i = 1:cardK
            n_beta_i = n.(beta{i});
            val1 = n_beta_i - sum(is_minor_sv(sigma{sigma_map.(beta{i})}.data));
            
            if n_beta_i > n_last.(beta{i})
                fprintf('▲');
            elseif n_beta_i == n_last.(beta{i})
                fprintf('%s',ALP{val1+1});
            else
                fprintf('▾');
            end
            
        end
        n_last = n;
        
        if INT_cg_count > 0
            fprintf(' | av_cg_st: %d',round(INT_cg/max(INT_cg_count))); % average cg steps required to reach tolerance
        end
        INT_cg = 0;
        INT_cg_count = 0;
        
        nres_y_scaled = nres_y*sqrt(scaling_L);
        if ~isempty(Lval)
            nres_yval_scaled = nres_yval*sqrt(scaling_Lval);
            if new_opt
                symbol = '*';
            else
                symbol = ' ';
            end
        else
            symbol = ' ';
        end
              
        fprintf(' |%siter: %4d, time: %2.2f',symbol,iter,toc);
        if ~strcmp(meta_para.opt_method,'none')
            fprintf(', sqrt(gamma): %.2e',sqrt(gamma));
        end
        fprintf(', diff_num_stable: %3d, quot_prod_stable: %.2e, f_sc: %.2e, nres_y_sc %.2e',num_stable_sv-num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,f_scaled,nres_y_scaled);
        if ~isempty(Lval)
            fprintf(', nres_yval_sc %.2e',nres_yval_scaled);
        end
        fprintf(', rel_nres_N_true: %.2e \n',nres_N_true/norm_Ntrue);
    end

    function plot_sing_spec()
        % plot all singular values and progress
        blue = [0,83,159]/255;
        teal = [0,152,161]/255;
        bord = [161,16,53]/255;
        viol = [97,33,88]/255;
        
        ms = 60;
        
        num_sv = 0;
        for j = 1:cardK
            maxsig = max([max(sigma{j}.data),sqrt(gamma),maxsig]);
            minsig = min([min(sigma{j}.data),sqrt(gamma),minsig]);
            num_sv = num_sv + length(sigma{j}.data);
        end
        
        y_max = 10*maxsig;
        y_min = minsig/100;
%         clf;
        hold off;
        
        semilogy([1,1],[y_min,y_max],':','color',[0.7,0.7,0.7]);
        hold on;
        
        left_marg = 0.2;
        
        nres_y_scaled = nres_y*sqrt(scaling_L);
        text(left_marg,nres_y_scaled,'~res_y ','HorizontalAlignment', 'right');
        semilogy([left_marg,cardK+1],[nres_y_scaled,nres_y_scaled],'--','color',bord);
        
        if ~isempty(Lval)
            nres_y_scaledval = nres_yval*sqrt(scaling_Lval);
            text(left_marg,nres_y_scaledval,'~res_yval ','HorizontalAlignment', 'right');
            semilogy([left_marg,cardK+1],[nres_y_scaledval,nres_y_scaledval],':','color',bord);
        end
        
        text(left_marg,nres_N_true,'res_N ','HorizontalAlignment', 'right');
        semilogy([left_marg,cardK+1],[nres_N_true,nres_N_true],'--','color',viol);
        
        if ~strcmp(meta_para.opt_method,'none')
            text(left_marg,sqrt(gamma),'\gamma ','HorizontalAlignment', 'right');
        end
        
        for k = min(iter-1,10):-1:1
            val = sqrt(Gamma(iter-k));
            semilogy([left_marg,cardK+1],[val,val],'-','color',1-1/k*(1-teal));
        end
        semilogy([left_marg,cardK+1],[sqrt(gamma),sqrt(gamma)],'-','color',teal);
        
        this_colormap = [blue; teal; viol];
        
        filled_sings = zeros(num_sv,3); num_filled = 0;
        unfilled_sings = zeros(num_sv,3); num_unfilled = 0;
        
        for j = 1:cardK
            semilogy([j,j],[y_min,y_max],':','color',[0.7,0.7,0.7]);
            
            sings = sigma{sigma_map.(beta{j})}.data;
            sings_true = sigma_true{sigma_map_true.(beta{j})}.data;
            
            min_le = min(length(sings),length(sings_true));
            
            is_near_true_sv = false(length(sings),1);
            
            is_near_true_sv(1:min_le) = abs(log(sings(1:min_le)) - log(sings_true(1:min_le))) < 1/10;
            is_near_true_sv(find(~is_near_true_sv,1)+1:end) = false;
            
            is_minor = is_minor_sv(sings);
            
            for k = 1:length(sings)
                if is_near_true_sv(k)
                    color = 1;
                else
                    color = 2;
                end
                
                if ~is_minor(k)
                    num_filled = num_filled + 1;
                    filled_sings(num_filled,:) = [j,sings(k),color];
                else
                    num_unfilled = num_unfilled + 1;
                    unfilled_sings(num_unfilled,:) = [j,sings(k),color];
                end
            end
        end
        if ~isempty(filled_sings)
            scatter(filled_sings(1:num_filled,1),filled_sings(1:num_filled,2),ms,filled_sings(1:num_filled,3),'filled');
        end
        if ~isempty(unfilled_sings)
            scatter(unfilled_sings(1:num_unfilled,1),unfilled_sings(1:num_unfilled,2),ms,unfilled_sings(1:num_unfilled,3));
        end
        scatter(violet_sings(1:num_violet,1),violet_sings(1:num_violet,2),1/4*ms,violet_sings(1:num_violet,3));
        set(gca, 'CLim', [1, 3])
        colormap(this_colormap);
        
        axis([-1 cardK+1 y_min y_max]);
        
        plotgca = gca; plotgca.XTick = 1:cardK;
        
        greekmn_beta = beta;
        for j = 1:cardK
            [~,number_start,number_end] = regexp(greekmn_beta{j},'\d*','Match');
            greekmn_beta{j} = ['\',greekmn_beta{j}(1:number_start-1),'{',greekmn_beta{j}(number_start:number_end),'}'];
        end
        plotgca.XTickLabel = greekmn_beta;
        
        ylabel('singular values');
        xlabel('inner mode names');
        drawnow
        
        if meta_para.record_movie
            iter_movie = iter_movie + 1;
            MOVIE(iter_movie) = getframe(gcf);
            save('movie','MOVIE');
        end
    end

%% explicitly adapt rank
    function logical_ind = is_minor_sv(sigma_unf)
        % determine which singular values count as minor ones
        logical_ind = meta_para.minor_sv_function(sigma_unf,gamma); %% TODO: replace num of minor sv by f_rank gamma estimate
    end

    function increase_rank = rank_of_edge_shall_be_increased(s_old,s_new)
        % determine if rank is to be increased
        mn_inter_s = mn_inter{s_old,s_new}; % = str_intersect(mN{s_old},mN{s_new});
        
        increase_rank = false;
        
        % if ~(condition)
        %   return
        % end
        %
        % means: after every such condition is met => increase_rank = true
        
        if ~(iter > 5 && n.(mn_inter_s) < meta_para.rN_uni_max)
            return;
        end
        
        if ~(minor_sing_val_count.(mn_inter_s) > meta_para.rank_increase_count_threshold)
            return
        end
        
        % check if rank increase is possible according to mode sizes:
        size_N_s_old = data_volume(N{s_old});
        size_N_s_new = data_volume(N{s_new});
        size_r_inter = n.(mn_inter_s);
        
        if ~((size_N_s_old/size_r_inter) >= (size_r_inter+1) && (size_N_s_new/size_r_inter) >= (size_r_inter+1))
            return;
        end
        
        increase_rank = true;
    end

    function decrease_rank = rank_of_edge_shall_be_decreased(s_old,s_new)
        % determine if rank is to be decreased
        mn_inter_s = mn_inter{s_old,s_new}; % = str_intersect(mN{s_old},mN{s_new});
        
        decrease_rank = false;
        
        % if ~(condition)
        %   return
        % end
        %
        % means: after every such condition is met => increase_rank = true
        
        if ~(iter > 5 && n.(mn_inter_s) > 1 + meta_para.num_minor_sv) 
            return;
        end
        
        if ~(minor_sing_val_count.(mn_inter_s) < -meta_para.rank_decrease_count_threshold)
            return
        end
        
        % check if rank decrease is possible according to mode sizes:
        size_N_s_old = data_volume(N{s_old});
        size_N_s_new = data_volume(N{s_new});
        size_r_inter = n.(mn_inter_s);
        
        if ~((size_N_s_old/size_r_inter) >= (size_r_inter-1) && (size_N_s_new/size_r_inter) >= (size_r_inter-1))
            return;
        end
        
        decrease_rank = true;
    end

    function increase_rank_of_edge(s_old,s_new)
        % increases rank using small singular value
        mn_inter_s = mn_inter{s_old,s_new};
        sigma_ind = sigma_map.(mn_inter_s);
        
        n_new = n;
        n_new.(mn_inter_s) = n_new.(mn_inter_s) + 1;
        n_new.(prime_rn.(mn_inter_s)) = n_new.(mn_inter_s);
        
        N{s_old} = add_orthogonal_column(N{s_old},mn_inter_s,n_new);
        N{s_new} = add_orthogonal_column(N{s_new},mn_inter_s,n_new);
        
        sigma{sigma_ind} = fold([unfold(sigma{sigma_ind}); meta_para.new_minor_sv_factor*sqrt(gamma)],sigma{sigma_ind}.mode_names,n_new);
        
        n = n_new;
        
        function U_new = add_orthogonal_column(U,mn_inter_s,n_new)
            % assumes U is orthogonal with respect to mn_inner_s
            beta_of_U = intersect(U.mode_names,beta);
            alpha_of_U = intersect(U.mode_names,alpha);
            
            other_inner_mn = str_setdiff(beta_of_U,{mn_inter_s});
            
            % add new part depending on if outer mode names exists or not
            U_unf = unfold(U,[{},alpha_of_U,other_inner_mn],mn_inter_s);
            q = zeros(size(U_unf,1),1);
            
            if ~isempty(alpha_of_U)
                q(:) = 1;
                q = q/norm(q);
            else
                q(1) = 1;
            end
            
            % orthogonalize (TWICE to deal with roundoff nresors)
            q = q - U_unf*(U_unf'*q); q = q/norm(q);
            q = q - U_unf*(U_unf'*q); q = q/norm(q);
            
            % add to U
            U_new_unf = [U_unf,q];
            U_new = fold(U_new_unf,[{},alpha_of_U,other_inner_mn,mn_inter_s],n_new);
        end
    end

    function decrease_rank_of_edge(s_old,s_new)
        % decrease rank by removing singular value parts
        mn_inter_s = mn_inter{s_old,s_new};
        sigma_ind = sigma_map.(mn_inter_s);
        
        n_new = n;
        n_new.(mn_inter_s) = n_new.(mn_inter_s) - 1;
        n_new.(prime_rn.(mn_inter_s)) = n_new.(mn_inter_s);
        
        % simply remove parts
        N{s_old} = node_part(N{s_old},mn_inter_s,1:n_new.(mn_inter_s));
        N{s_new} = node_part(N{s_new},mn_inter_s,1:n_new.(mn_inter_s));
        sigma{sigma_ind} = node_part(sigma{sigma_ind},mn_inter_s,1:n_new.(mn_inter_s));
        
        n = n_new;
    end

%% cg solver for recursive path evaluations
    function path_rec_cg_solver()
        % approximately solve H x = u
        Neigh_root = neighbors(G,root)';
        u = add_scale(scaling_L,boxtimes(branchLN{root}{Neigh_root},L{root},y));
        
        x0 = N{root};
        
        leN = numel(Neigh_root);
        
        %% preconditioner (diagonal):
        diag_NWN_branches = cell(1,leN);
        for j = 1:leN
            [~,other_mn_of_c] = str_intersect(mN{root},mN{Neigh_root(j)});
            ones_other = ones_node(other_mn_of_c,n);
            diag_NWN_branches{j} = boxtimes(node_diag(branchW{Neigh_root(j)}),ones_other);
        end
        diag_NWN = add_scale(diag_NWN_branches{:});
        
        diag_LNTLN_branches = cell(1,leN);
        for j = 1:leN
            branchLNci = branchLN{root}{Neigh_root(j)};
            diag_LNTLN_branches{j} = boxtimes(branchLNci,branchLNci,'^',branchLNci.mode_names);
        end
        diag_LNTLN_Lc = boxtimes(L{root},L{root},'^',L{root}.mode_names);
        diag_LNTLN = boxtimes(diag_LNTLN_branches{:},diag_LNTLN_Lc,'mode','optimal');
        
        if ~isempty(B)
            diag_NBN_branches = cell(1,leN);
            for j = 1:leN
                diag_NBN_branches{j} = node_diag(derive_net(prime_rn_inv,branchNBN_pr{root}{Neigh_root(j)}));
            end
            diag_Bc = node_diag(B{root});
            diag_NBN = boxtimes(diag_NBN_branches{:},diag_Bc,'mode','optimal');
            
            % combine
            diagR = add_scale(scaling_L,diag_LNTLN,scaling_W,diag_NWN,scaling_B,diag_NBN);
        else
            diagR = add_scale(scaling_L,diag_LNTLN,scaling_W,diag_NWN);
        end
        
        % and invert:
        diagR_unf = unfold(diagR,x0.mode_names);
        diagRinv = fold(diagR_unf.^(-1),x0.mode_names,n);
        
        %% main CG:
        % multiply with system matrix
        Hx0 = multiply_with_system_matrix(x0,Neigh_root);
        
        % u - LNTLN x0
        grad = add_scale(u,-1,Hx0);
        
        % apply preconditioner:
        deltag = boxtimes(grad,diagRinv,'^',grad.mode_names);
        
        LNTLN_deltag = multiply_with_system_matrix(deltag,Neigh_root);
        
        % calculate alpha
        zkTrk = get_data(boxtimes(deltag,grad));
        sqrtzkTrk0 = sqrt(zkTrk);
        
        pkTApk = get_data(boxtimes(deltag,LNTLN_deltag));
        if pkTApk == 0
            cg_alpha = 0;
        else
            cg_alpha = zkTrk/pkTApk;
        end
        
        % adapt x0
        x0 = add_scale(x0,cg_alpha,deltag); % x0 = x0 + cg_alpha*deltag;
        
        int_cg = 1;
        tol_reached = 0;
        max_cg_iter = numel(x0.data);
        
        while ~tol_reached && int_cg < max_cg_iter
            Hx0 = add_scale(Hx0,cg_alpha,LNTLN_deltag);
            grad = add_scale(u,-1,Hx0);
            
            % save previous values
            zkTrk_last = zkTrk;
            deltag0 = deltag;
            
            deltag = boxtimes(grad,diagRinv,'^',grad.mode_names);
            
            zkTrk = get_data(boxtimes(deltag,grad)); % zkTrk = deltag(:)'*grad(:);
            
            % breaking criterion
            if sqrt(zkTrk) > meta_para.cg_break_fac*sqrtzkTrk0 && sqrt(zkTrk) > meta_para.absolute_cg_break_tol
                % calculate correction with factor cg_beta
                cg_beta = zkTrk / zkTrk_last;
                deltag = add_scale(deltag,cg_beta,deltag0); % deltag = deltag + cg_beta*deltag0;
                
                LNTLN_deltag = multiply_with_system_matrix(deltag,Neigh_root);
                
                pkTApk = get_data(boxtimes(deltag,LNTLN_deltag));
                cg_alpha = zkTrk/pkTApk;
                
                % adapt x0 and residual
                x0 = add_scale(x0,cg_alpha,deltag); % x0 = x0 + cg_alpha*deltag;
                
                % count cg iterations
                int_cg = int_cg + 1;
            else
                tol_reached = 1;
            end
            
            INT_cg = INT_cg + int_cg;
            INT_cg_count = INT_cg_count + 1;
        end
        
        N{root} = x0;
    end

    function Hx = multiply_with_system_matrix(x,Neigh_root)
        % component-wise multiplication with system matrix H in cg algorithm
        A_N = boxtimes(branchLN{root}{Neigh_root},L{root},x,'^',zeta,'mode','optimal');
        LNTLN_N = boxtimes(branchLN{root}{Neigh_root},L{root},A_N,'mode','optimal');
        
        branchWhx = cell(1,numel(Neigh_root));
        for i = 1:numel(Neigh_root)
            branchWhx{i} = boxtimes(branchW{Neigh_root(i)},x,'mode','optimal');
        end
        NWN_N = add_scale(branchWhx{:});
        % (dubm and wrong:) NWN_N = boxtimes(branchW{Neigh_root},x,'mode','optimal');
        
        if ~isempty(B)
            NBN_N = boxtimes(derive_net(prime_rn,x),branchNBN_pr{root}{Neigh_root},B{root},'mode','optimal');
            Hx = add_scale(scaling_L,LNTLN_N,scaling_W,NWN_N,scaling_B,NBN_N);
        else
            Hx = add_scale(scaling_L,LNTLN_N,scaling_W,NWN_N);
        end
    end

end











