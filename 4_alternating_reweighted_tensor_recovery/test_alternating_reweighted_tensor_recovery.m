function test_alternating_reweighted_tensor_recovery()

%% parameters
d = 4;
n_uni = 5;

% problem:
problem_setting.n_values = n_uni*ones(1,d);
problem_setting.Ltype = 'gaussian';
problem_setting.rL = 1; % is set equal to 1 for problem.Ltype = 'sampling'
problem_setting.ell_fct = @(n_,r,vol) 1.1*vol;

problem_setting.HTmathcalK = binary_tree(1:d);
% problem_setting.HTmathcalK = linear_tree(1:d);
% problem_setting.HTmathcalK = tucker_tree(1:d);

% validation:
validation_setting.ratio = 0.15;

% reference solution:
solution_setting.rNtrue = min(n_uni,4);
solution_setting.exp_decay_sv = 0; % 0: no exponential decay
solution_setting.HTmathcalKtrue = problem_setting.HTmathcalK;

% important meta parameters
meta_para.rN_uni_max = 8;
meta_para.f_gamma = 1.01;

if strcmp(problem_setting.Ltype,'sampling')
    problem_setting.rL = 1;
end

%% get problem
[problem,solution] = get_network_tensor_recovery_prob_sol(problem_setting,solution_setting);
y = problem.y;
L = problem.L;
Ntrue = solution.Ntrue;

%% split off validation
[problem,validation] = split_off_validation(problem,validation_setting);

%% run (p = 0)
meta_para.weight_p = 0;
problem.B = []; 
problem.lambda = 0;

N_alg = alternating_reweighted_tensor_recovery(problem,validation,solution,meta_para);
net_dist(N_alg,Ntrue)
net_dist({L,N_alg},y)

%% evaluate
accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_N_true] = evaluate_network_tensor(L,N_alg,Ntrue,y,accuracy);
fprintf('p = 0 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_X_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_N_true);