function [problem,validation] = split_off_validation(problem,validation_setting)

n = problem.n;
ell = problem.n.(problem.zeta);
N = prod(cell2mat(getfields(n,problem.alpha)));
L_new = problem.L;
y_new = problem.y;

%% network Lval
if validation_setting.ratio > 0
    Lval = L_new;
    ellval = ceil(validation_setting.ratio*ell);
    select_val = sort(datasample(1:ell,ellval,'Replace',false));
    select_samp = sort(setdiff(1:ell,select_val));
    
    ell = ell - ellval;
    for i = 1:length(L_new)-2
        L_new{i} = node_part(problem.L{i},'zeta',select_samp);
        Lval{i} = node_part(problem.L{i},'zeta',select_val);
    end
    y_new = node_part(problem.y,'zeta',select_samp);
    yval = node_part(problem.y,'zeta',select_val);
        
    fprintf('Training measurement ratio of %.2e%%.\n',ell/N*100);
    fprintf('Validation measurement ratio of %.2e%% (method assumes that measurement vector is not missleading). \n',ellval/N*100);
else   
    fprintf('Validation not applied.\n');
    
    Lval = [];
    yval = [];
end

%% transfer
n.(problem.zeta) = ell;
problem.y = y_new;
problem.L = L_new;

validation.yval = yval;
validation.Lval = Lval;


