function [problem,solution] = get_matrix_recovery_prob_sol(problem_setting,solution_setting)

%% sample solution 
Xtrue = randn(problem_setting.n(1),solution_setting.rXtrue)*randn(solution_setting.rXtrue,problem_setting.n(2));

%% operator L
ell = ceil(min(prod(problem_setting.n),problem_setting.ell_fct(problem_setting.n,solution_setting.rXtrue,sum(problem_setting.n)*solution_setting.rXtrue)));

switch problem_setting.Ltype
    case 'gaussian'
        Lbar = randn(ell,prod(problem_setting.n));
        Lbar = Lbar/norm(Lbar,'fro');
        
    case 'sampling'
        k_min = 0;
        while k_min < solution_setting.rXtrue
            I = eye(prod(problem_setting.n));
            samps = sort(datasample(1:prod(problem_setting.n),ell,'Replace',false));
            Lbar = I(samps,:);
            Z = zeros(problem_setting.n);
            Z(samps) = 1;
            k_min = min([sum(Z,1),sum(Z,2)']);
        end       
end

%% measurements y
y = Lbar*Xtrue(:);

%% transfer
problem = struct;
solution = struct;

problem.n = problem_setting.n;
problem.Lbar = Lbar;
problem.y = y;
problem.ell = ell;

solution.Xtrue = Xtrue;