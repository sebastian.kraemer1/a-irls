function [X,info] = reweighted_matrix_recovery(problem,solution,meta_para)

close all

    function suggest_default(field,default)
        if ~isfield(meta_para,field)
            meta_para.(field) = default;
        end
    end

%% Parameter defaults (preferences)

% weight parameter p
suggest_default('weight_p',0);
if meta_para.weight_p ~= 0
    warning('not using rank weights (p = 0) but p = %f',meta_para.weight_p);
end

% preferences
suggest_default('iter_max',10000000); % will break earlier by itself
suggest_default('print_interval',1000);
suggest_default('plot_interval',1000);
suggest_default('f_gamma',1.01);
suggest_default('opt_method','image');
suggest_default('gamma_decline_method','constant');
suggest_default('use_sv_stagnation_criterion',false);

% breaking criteria
suggest_default('rel_gamma_min',1e-10);
suggest_default('nres_min',0.8e-6);

suggest_default('breaking_conditions','elaborate');
suggest_default('rel_sigma_min',1e-10);
suggest_default('sigma_gamma_factor',10);
suggest_default('rel_y_min',1e-12);

% heuristic, well working choices
suggest_default('omega',0.5);
suggest_default('marginal_factor',1e-12);
suggest_default('gamma_start_factor',10);
suggest_default('image_kernel_switch',1e-6);

if ~meta_para.use_sv_stagnation_criterion && strcmp(meta_para.gamma_decline_method,'hard_use_sv') % && meta_para.use_sv_offset > 1
    warning('using sv stagnation criterion, and otherwise only plain breaking critera, due to hard use of sv gamma decline method');
    meta_para.use_sv_stagnation_criterion = true;
    meta_para.breaking_conditions = 'plain';
end

%% read problem setting
n = problem.n;
Lbar = problem.Lbar;
y = problem.y;

Xtrue = solution.Xtrue;

% magnitudes
norm_Lbar = norm(Lbar,'fro');
scaling_L = prod(n)/norm_Lbar^2;
ell = size(Lbar,1);
norm_y = norm(y);

%% svd of Xtrue
sigma_true = svd(Xtrue,0);
ind_stable_true = sigma_true/norm(Xtrue,'fro') > meta_para.marginal_factor;
num_stable_sv_true = sum(ind_stable_true);
prod_stable_sv_true = prod(sigma_true(ind_stable_true));
sigma_true_stab = sigma_true(ind_stable_true);

norm_Xtrue = norm(sigma_true);

%% for method kernel
Kbar = null(Lbar);

%% for method relaxed
omega = meta_para.omega;

%% init X
X = reshape(lsqminnorm(Lbar,y),n);
X0 = X;

gamma = meta_para.gamma_start_factor*norm(X,'fro')^2;
Sigma = [];
Gamma = [];
Res_y = [];

% singular values
[U,S,V] = svd(X,0);
sigma = diag(S);

%% colors for plot
blue = [0,83,159]/255;
teal = [0,152,161]/255;
bord = [161,16,53]/255;
viol = [97,33,88]/255;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
tic

LbarTreshape = reshape(Lbar',n(1),[]);
opt_method = meta_para.opt_method;

for iter = 1:meta_para.iter_max
    %% update iterate
    switch opt_method
        case 'kernel'
            W = get_W_tp(U,sigma,gamma,meta_para.weight_p/2-1);
            Wbar = kron(eye(n(2)),W);
            
            X(:) = X0(:) - Kbar* ( (Kbar'*Wbar*Kbar) \ (Kbar'*Wbar*X0(:)) );
            
        case 'image'
            if meta_para.weight_p == 0
                Wm = X*X' + gamma*eye(n(1));
            else
                Wm = get_W_tp(U,sigma,gamma,1-meta_para.weight_p/2);
            end
            
            WLT = reshape(Wm*LbarTreshape,[prod(n),ell]);
            X(:) = WLT* ( (Lbar*WLT) \ y );
            
        case 'image_optimal' % as by C.Kuemmerle
            W_Lm = get_W_tp(U,sigma,gamma,(1-meta_para.weight_p/2)/2);
            W_Rm = get_W_tp(V,sigma,gamma,(1-meta_para.weight_p/2)/2);
            
            Winv = kron(W_Rm,W_Lm);
            
            WLT = Winv*Lbar';
            X(:) = WLT* ( (Lbar*WLT) \ y );   
            
        case 'kernel_optimal' % same update as image_optimal
            W_L = get_W_tp(U,sigma,gamma,-(1-meta_para.weight_p/2)/2);
            W_R = get_W_tp(V,sigma,gamma,-(1-meta_para.weight_p/2)/2);
            
            Wbar = kron(W_R,W_L);
            
            X(:) = X0(:) - Kbar* ( (Kbar'*Wbar*Kbar) \ (Kbar'*Wbar*X0(:)) );            
            
        case 'image_both_sided'
            W1 = get_W_tp(U,sigma,gamma,meta_para.weight_p/2-1);
            W2 = get_W_tp(V,sigma,gamma,meta_para.weight_p/2-1);
            
            W1bar = kron(eye(n(2)),W1);
            W2bar = kron(W2,eye(n(1)));
            Wbar = W1bar + W2bar;
            WLT = Wbar \ Lbar';
            
            X(:) = WLT* ( (Lbar*WLT) \ y );
            
        case 'relaxed'
            W = get_W_tp(U,sigma,gamma,meta_para.weight_p/2-1);
            Wbar = kron(eye(n(2)),W);
            
            scaling_W = omega^2*gamma;
            H = scaling_L*(Lbar'*Lbar) + scaling_W*Wbar;
            u = scaling_L*Lbar'*y;
            X(:) = H \ u;
    end
    
    %% singular values
    [U,S,V] = svd(X,0);
    sigma = diag(S);
    
    %% evaluate progress
    ind_stable = sigma > sqrt(gamma);
    last_stable_sv = sigma(find(~ind_stable,1));
    
    nres_X_true = norm(X-Xtrue,'fro');
    nres_y = norm(Lbar*X(:)-y);
    nres_y_scaled = nres_y*sqrt(scaling_L);
    rel_nres_y = nres_y/norm_y;
    
    switch opt_method
        case 'relaxed'
            f_scaled = sqrt(scaling_L*nres_y^2 + scaling_W*(log(prod(sigma.^2+gamma)) - length(sigma)*log(gamma)));
        otherwise
            f_scaled = gamma*(log(prod(sigma.^2+gamma)) - length(sigma)*log(gamma));
    end
    
    num_stable_sv = sum(ind_stable);
    prod_stable_sv = prod(sigma(ind_stable));
    
    % rather experimental, more elaborate evaluation
    marginal_sv = sigma/norm(sigma) < meta_para.marginal_factor;
    sigmoidal_evaluation = (1-gamma./sigma.^2.*log(1+sigma.^2/gamma)) .* ~marginal_sv;
    continuous_num_stable_sv = sum(sigmoidal_evaluation);
    continuous_prod_stable_sv = prod(sigma.^(sigmoidal_evaluation));
    
    sigmoidal_evaluation_true = 1-log((1+sigma_true_stab.^2/gamma).^(gamma./sigma_true_stab.^2));
    continuous_num_stable_sv_true = sum(sigmoidal_evaluation_true);
    continuous_prod_stable_sv_true = prod(sigma_true_stab.^(sigmoidal_evaluation_true));
    
    %% print
    print_input = {iter,toc,sqrt(gamma),last_stable_sv/sqrt(gamma),num_stable_sv,num_stable_sv_true,continuous_num_stable_sv-continuous_num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,continuous_prod_stable_sv/continuous_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true/norm_Xtrue};
    if ~mod(iter,meta_para.print_interval)
        print_progress(print_input{:});
    end
    
    %% plot
    if ~mod(iter,meta_para.plot_interval)
        Sigma = [Sigma; sigma(:)']; %#ok<AGROW>
        Gamma = [Gamma; sqrt(gamma)]; %#ok<AGROW>
        
        hold off;
        
        for i = 1:size(Sigma,2)
            semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Sigma(:,i),'-','color',blue);
            hold on;
        end
        for i = 1:num_stable_sv_true
            semilogy([meta_para.plot_interval,iter],[sigma_true(i),sigma_true(i)],'--','color',viol);
        end
        
        semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Gamma(:),'-','color',teal);
        
        if strcmp(opt_method,'relaxed')
            Res_y = [Res_y; nres_y_scaled]; %#ok<AGROW>
            semilogy(meta_para.plot_interval:meta_para.plot_interval:iter,Res_y(:),':','color',bord);
        end
        
        drawnow
    end
    
    %% consider break
    switch meta_para.breaking_conditions
        case 'plain'
            if sqrt(gamma)/norm(sigma) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
                print_progress(print_input{:});
                break;
            end
        case 'elaborate'
            if sigma(num_stable_sv_true+1) < meta_para.rel_sigma_min*norm(sigma_true) && rel_nres_y < meta_para.rel_y_min || sqrt(gamma)/norm(sigma) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
                print_progress(print_input{:});
                break;
            end
            if num_stable_sv > num_stable_sv_true && sigma(num_stable_sv_true+1) > meta_para.sigma_gamma_factor*sqrt(gamma)
                fprintf('Improvement is not to be expected. Stopping.\n');
                print_progress(print_input{:});
                break;
            end
    end
    
    if meta_para.use_sv_stagnation_criterion
        if iter > 100 && gamma_old/gamma < meta_para.f_gamma ... % (the second condition is actually redundant through the third one)
                && max(abs(sigma(1:num_stable_sv_true+meta_para.use_sv_offset)./sigma_old(1:num_stable_sv_true+meta_para.use_sv_offset)-1)) < meta_para.f_gamma^2-1
            fprintf('Decline of gamma stagnating. Stopping.\n');
            print_progress(print_input{:});
            break;
        end
        sigma_old = sigma;
        gamma_old = gamma;
    end
    
    %% decrease gamma
    switch meta_para.gamma_decline_method
        case 'constant'
            gamma = gamma / meta_para.f_gamma;
        case 'soft_use_sv'
            if iter > 5
                gamma = min(gamma / meta_para.f_gamma, sigma(num_stable_sv_true+meta_para.use_sv_offset)^2);
            end
        case 'hard_use_sv'
            gamma_old = gamma;
            if iter > 5
                gamma = min(gamma, sigma(num_stable_sv_true+meta_para.use_sv_offset)^2);
            end
            
    end
    
    %% change method to avoid ill-conditioning
    if sqrt(gamma)/norm(sigma) < meta_para.image_kernel_switch 
        switch opt_method
            case 'image'
                opt_method = 'kernel';
            case 'image_optimal'
                opt_method = 'kernel_optimal';
        end
    end  
    
end

info = struct;
info.gamma = gamma;
info.meta_para = meta_para;
info.time = toc;
info.iter = iter;

end

function print_progress(iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,continuous_num_stable_sv_minus_continuous_num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,continuous_prod_stable_sv_over_continuous_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue)

fprintf('iter: %4d, time: %2.2f, sqrt(gamma): %.2e, quot_gamma_stable: %.2e, num_stable: %d/%d (%4.2f), quot_prod_stable: %.2e (%.2e), f_sc: %.2e, rel_nres_y: %.2e, rel_nres_X_true: %.2e \n',...
    iter,toc,sqrt_gamma,last_stable_sv_over_sqrt_gamma,num_stable_sv,num_stable_sv_true,continuous_num_stable_sv_minus_continuous_num_stable_sv_true,prod_stable_sv_over_prod_stable_sv_true,continuous_prod_stable_sv_over_continuous_prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true_over_norm_Xtrue);

end

function W = get_W_tp(U,sigma,gamma,q)

W = U*diag((sigma.^2+gamma).^q-gamma.^q)*U' + gamma.^q*eye(size(U,1));

end


