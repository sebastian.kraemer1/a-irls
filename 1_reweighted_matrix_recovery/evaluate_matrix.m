function [success,diff,quot,rel_nres_y,rel_nres_X_true,sigma,sigma_true] = evaluate_matrix(Lbar,X,Xtrue,y,accuracy)

success = false;

%% rank and singular values with respect to accuracy
[num_stable_sv,prod_stable_sv,sigma] = eval_this(X,accuracy);
[num_stable_sv_true,prod_stable_sv_true,sigma_true] = eval_this(Xtrue,accuracy);

%% difference and quotient
diff = num_stable_sv - num_stable_sv_true;
quot = prod_stable_sv / prod_stable_sv_true;

%% residuals
rel_nres_y = norm(Lbar*X(:)-y)/norm(y);
rel_nres_X_true = norm(X-Xtrue,'fro')/norm(sigma_true);

%% check whether success (1) or improvement (2)
if rel_nres_y < accuracy
    if diff == 0 && quot > 0.98 && quot < 1.005
        success = true;
    end
    if diff < 0 || diff == 0 && quot <= 0.98
        success = 2;
    end
end

end

function [num_stable_sv,prod_stable_sv,sigma] = eval_this(X,accuracy)

sigma = svd(X,0);

ind_stable = sigma > accuracy*norm(sigma);
num_stable_sv = sum(ind_stable);
prod_stable_sv = prod(sigma(ind_stable));

end