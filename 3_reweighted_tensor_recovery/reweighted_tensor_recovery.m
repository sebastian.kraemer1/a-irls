function [X,info] = reweighted_tensor_recovery(problem,solution,meta_para)

close all
activate_boxtimes_mem(10000)

    function suggest_default(field,default)
        if ~isfield(meta_para,field)
            meta_para.(field) = default;
        end
    end

%% Parameter defaults (preferences)

% weight parameter p
suggest_default('weight_p',0);
if meta_para.weight_p ~= 0
    warning('not using rank weights (p = 0) but p = %f',meta_para.weight_p);
end

% preferences
suggest_default('iter_max',1000000); % will break earlier by itself
suggest_default('print_interval',100);
suggest_default('plot_interval',100);
suggest_default('f_gamma',1.01);
suggest_default('opt_method','image');

% breaking criteria
suggest_default('rel_gamma_min',5e-8); % 1e-7
suggest_default('nres_min',0.8e-6);

% heuristic, well working choices
suggest_default('omega',0.5);
suggest_default('marginal_factor',1e-12);
suggest_default('image_kernel_switch',1e-6);

%% read problem setting
d = problem.d;
n = problem.n;
L = problem.L;
y = problem.y;
mathcalK = problem.mathcalK;
alpha = problem.alpha;
zeta = problem.zeta;

Xtrue = solution.Xtrue;
norm_y = net_norm(y);

%% mathcalK
cardK = length(mathcalK);
alphaK = cell(1,cardK);
for i = 1:cardK
    alphaK{i} = alpha(mathcalK{i});
end
beta = mna('beta',1:cardK);

%% svd of Xtrue
[~,sigma_true_unfold] = mathcalK_singular_values(Xtrue,alphaK,beta);
norm_Xtrue = net_norm(Xtrue);
num_stable_sv_true = 0;
prod_stable_sv_true = 1;
for i = 1:length(sigma_true_unfold)
    ind_stable = sigma_true_unfold{i} > meta_para.marginal_factor;
    num_stable_sv_true = num_stable_sv_true + sum(ind_stable);
    prod_stable_sv_true = prod_stable_sv_true*prod(sigma_true_unfold{i}(ind_stable));
end

%% delta tensors
[delta_J,delta_Jc,node_eye,mathcalKc] = deal(cell(1,cardK));

for i = 1:d
    node_eye{i} = fold(eye(n.(alpha{i})),[alpha(i),alpha(i)],n);
end

for i = 1:cardK
    delta_J{i} = boxtimes(node_eye{mathcalK{i}});
    
    mathcalKc{i} = setdiff(1:d,mathcalK{i});
    delta_Jc{i} = boxtimes(node_eye{mathcalKc{i}});
end

%% init X
X = fold(lsqminnorm(unfold(L,zeta,alpha),unfold(y,zeta)),alpha,n);

gamma = net_norm(X)^2;
WJs = deal(cell(1,cardK));

%% for opt_method 'kernel'
K_ = null(unfold(L,zeta,alpha));
tau = 'tau';
n.(tau) = size(K_,2);
K = fold(K_,[alpha,tau],n);

%% for opt_method 'relaxed'
prod_n = data_volume(X);
omega = meta_para.omega;
scaling_L = prod_n/net_norm(L)^2;

%% main %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %% %%
tic;

opt_method = meta_para.opt_method;

for iter = 1:meta_para.iter_max
    %% singular values
    [U,sigma_unfold] = mathcalK_singular_values(X,alphaK,beta);
    
    %% get weight matrix W(hat)
    X0 = X;
    for i = 1:cardK
        offset = gamma^(-(1-meta_para.weight_p/2)); % only important if n_J > n_Jc
        s_gamma_inv = fold((sigma_unfold{i}.^2+gamma).^(-(1-meta_para.weight_p/2))-offset,beta{i},length(sigma_unfold{i}));
        WJs{i} = boxtimes( add_scale(boxtimes(U{i},s_gamma_inv,U{i},'_',beta{i}),offset,delta_J{i}), delta_Jc{i});
    end
    What = add_scale(WJs{:});
    
    %% update iterate
    switch opt_method
        case 'kernel'
            H = boxtimes(K,What,K,'_',alpha);
            b = boxtimes(K,What,X0,'_',alpha);
            
            b.data = -b.data;
            
            x = node_linsolve(H,b);
            X = add_scale(X0,boxtimes(K,x));
            
        case 'image'
            What_invL = node_linsolve(What,L);
            LWhat_invL = boxtimes(L,What_invL,'_',alpha);
            
            X = boxtimes(What_invL,node_linsolve(LWhat_invL,y));
            
        case 'relaxed'
            scaling_W = 1/cardK*omega^2*gamma;
            H = add_scale(scaling_L,boxtimes(L,L,'_',zeta),scaling_W,What);
            u = add_scale(scaling_L,boxtimes(y,L));
            X = node_linsolve(H,u);
    end
    
    %% evaluate progress
    log_sum = 0;
    sumnJ = 0;
    num_stable_sv = 0;
    prod_stable_sv = 1;
    for i = 1:length(sigma_unfold)
        log_sum = log_sum + sum(log(sigma_unfold{i}.^2+gamma));
        ind_stable = sigma_unfold{i} > sqrt(gamma);
        num_stable_sv = num_stable_sv + sum(ind_stable);
        prod_stable_sv = prod_stable_sv*prod(sigma_unfold{i}(ind_stable));
        sumnJ = sumnJ + length(sigma_unfold{i});
    end
    
    nres_X_true = net_dist(X,Xtrue);
    nres_y = net_dist({L,X},y);
    nres_y_scaled = nres_y*sqrt(scaling_L);
    rel_nres_y = nres_y/norm_y;
    
    switch opt_method
        case 'relaxed'
            f_scaled = sqrt(scaling_L*nres_y^2 + scaling_W*(log_sum - sumnJ*log(gamma)));
            
        otherwise
            f_scaled = 1/cardK*gamma*(log_sum - sumnJ*log(gamma));
    end
    
    %% print
    if ~mod(iter,meta_para.print_interval)
        fprintf('iter: %4d, time: %2.2f, sqrt(gamma): %.2e, diff_num_stable: %3d, quot_prod_stable: %.2e, f_sc: %.2e, rel_nres_y %.2e, rel_nres_X_true: %.2e \n',...
            iter,toc,sqrt(gamma),num_stable_sv-num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true/norm_Xtrue);
    end
    
    %% plot
    if ~mod(iter,meta_para.plot_interval)
        plot_sing_spec(gamma,sigma_unfold,sigma_true_unfold,mathcalK,nres_y_scaled,nres_X_true);
    end
    
    %% consider break
    if sqrt(gamma)/net_norm(X) < meta_para.rel_gamma_min || nres_X_true/norm_Xtrue < meta_para.nres_min
        fprintf('iter: %4d, time: %2.2f, sqrt(gamma): %.2e, diff_num_stable: %3d, quot_prod_stable: %.2e, f_sc: %.2e, rel_nres_y %.2e, rel_nres_X_true: %.2e \n',...
            iter,toc,sqrt(gamma),num_stable_sv-num_stable_sv_true,prod_stable_sv/prod_stable_sv_true,f_scaled,rel_nres_y,nres_X_true/norm_Xtrue);        
        break;
    end
    
    %% decrease gamma
    gamma = gamma / meta_para.f_gamma;
    
    %% possibly change method to avoid ill-conditioning
    if sqrt(gamma)/net_norm(X) < meta_para.image_kernel_switch && strcmp(opt_method,'image')
        opt_method = 'kernel';
    end
    
end

info = struct;
info.sigma_unfold = sigma_unfold;
info.gamma = gamma;
info.meta_para = meta_para;
info.time = toc;
info.iter = iter;

end

%% subfunctions
function [U,sigma_unfold] = mathcalK_singular_values(X,alphaK,beta)
[U,sigma_unfold] = deal(cell(1,length(alphaK)));

for i = 1:length(alphaK)
    [U{i},sigma_i,~] = node_svd(X,alphaK{i},beta{i},-1);
    sigma_unfold{i} = unfold(sigma_i);
end
end

function plot_sing_spec(gamma,sigma_unfold,sigma_true_unfold,mathcalK,nres_y_scaled,nres_X_true)
% plot all singular values and progress
cardK = length(mathcalK);

blue = [0,83,159]/255;
teal = [0,152,161]/255;
bord = [161,16,53]/255;
viol = [97,33,88]/255;

ms = 60;

maxsig = 0;
minsig = Inf;
for j = 1:cardK
    maxsig = max([max(sigma_unfold{j}),maxsig]);
    minsig = min([sigma_unfold{j}(1),min(sigma_unfold{j}(sigma_unfold{j}>sqrt(gamma))),minsig]);
end

y_max = max(10*maxsig,sqrt(gamma));
y_min = min([sqrt(gamma)/10,y_max/10,minsig/10]);
% clf;
hold off;

semilogy([1,1],[y_min,y_max],':','color',[0.7,0.7,0.7]);
hold on;

left_marg = 0.2;

text(left_marg,nres_y_scaled,'~res_y ','HorizontalAlignment', 'right');
semilogy([left_marg,cardK+1],[nres_y_scaled,nres_y_scaled],'--','color',bord);

text(left_marg,nres_X_true,'res_N ','HorizontalAlignment', 'right');
semilogy([left_marg,cardK+1],[nres_X_true,nres_X_true],'--','color',viol);

text(left_marg,sqrt(gamma),'\gamma ','HorizontalAlignment', 'right');
semilogy([left_marg,cardK+1],[sqrt(gamma),sqrt(gamma)],'-','color',teal);

for j = 1:cardK
    semilogy([j,j],[y_min,y_max],':','color',[0.7,0.7,0.7]);
end

this_colormap = [blue; teal; viol];

filled_sings = [];
unfilled_sings = [];
violet_sings = [];

for j = 1:cardK
    sings = sigma_unfold{j};
    sings_true = sigma_true_unfold{j};
    
    min_le = min(length(sings),length(sings_true));
    
    is_near_true_sv = false(length(sings),1);
    
    is_near_true_sv(1:min_le) = abs(log(sings(1:min_le)) - log(sings_true(1:min_le))) < 1/10;
    is_near_true_sv(find(~is_near_true_sv,1)+1:end) = false;
    
    is_minor = sings < sqrt(gamma);
    
    for k = 1:length(sings)
        if is_near_true_sv(k)
            color = 1;
        else
            color = 2;
        end
        
        if ~is_minor(k)
            filled_sings = [filled_sings; [j,sings(k),color]]; %#ok<AGROW>
        else
            unfilled_sings = [unfilled_sings; [j,sings(k),color]]; %#ok<AGROW>
        end
    end
    violet_sings = [violet_sings; [repmat(j,[length(sings_true),1]),sings_true,repmat(3,[length(sings_true),1])]]; %#ok<AGROW>
end

if ~isempty(filled_sings)
    scatter(filled_sings(:,1),filled_sings(:,2),ms,filled_sings(:,3),'filled');
end
if ~isempty(unfilled_sings)
    scatter(unfilled_sings(:,1),unfilled_sings(:,2),ms,unfilled_sings(:,3));
end
scatter(violet_sings(:,1),violet_sings(:,2),1/4*ms,violet_sings(:,3));
set(gca, 'CLim', [1, 3])
colormap(this_colormap);

axis([-1 cardK+1 y_min y_max]);

plotgca = gca; plotgca.XTick = 1:cardK;

XTickLabel = cell(1,cardK);
for j = 1:cardK
    XTickLabel{j} = ['{',sprintf('%d',mathcalK{j}),'}'];
end
plotgca.XTickLabel = XTickLabel;

ylabel('singular values');
xlabel('J in K');
drawnow
end

