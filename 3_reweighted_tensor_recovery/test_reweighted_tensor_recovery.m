function test_reweighted_tensor_recovery()

%% parameters
d = 4;
n_uni = 5; 

% problem:
problem_setting.n_values = n_uni*ones(1,d);
problem_setting.Ltype = 'gaussian';
problem_setting.ell_fct = @(n,r,vol) 1.1*vol; 

% reference solution:
solution_setting.rXtrue = min(n_uni,4);
solution_setting.Xtruetype = 'HT';

% important meta parameters
meta_para.opt_method = 'image';
meta_para.f_gamma = 1.01;

% family of matricizations (problem)
mathcaltype = solution_setting.Xtruetype;

switch mathcaltype
    case 'CP'
        switch problem_setting.d
            case 4
                mathcalK = {1,2,3,4,[1,2],[1,3],[1,4],[2,3],[2,4],[3,4],[2,3,4],[1,3,4],[1,2,4],[1,2,3]};
            case 3
                mathcalK = {1,2,3,[1,2],[1,3],[2,3]};
        end
    case 'HT'
        mathcalK = binary_tree(1:d);
        solution_setting.HTmathcalKtrue = mathcalK;
end
problem_setting.mathcalK = mathcalK;

%% get problem setting
[problem,solution] = get_full_tensor_recovery_prob_sol(problem_setting,solution_setting);

%% run (p = 0)
meta_para.weight_p = 0;
X_alg = reweighted_tensor_recovery(problem,solution,meta_para);

%% evaluate
accuracy = 1e-6;
[success,diff,quot,rel_nres_y,rel_nres_X_true] = evaluate_full_tensor(problem.L,X_alg,solution.Xtrue,problem.y,problem.alpha,problem.mathcalK,accuracy);
fprintf('p = 0 | success: %d, diff_rank_eps: %.2e, quot_eps: %.2e, residual_y: %.2e, residual_X_true: %.2e \n',success,diff,quot,rel_nres_y,rel_nres_X_true);