function [success,diff,quot,rel_nres_y,rel_nres_X_true,sigma,sigma_true] = evaluate_full_tensor(L,X,Xtrue,y,alpha,mathcalK,accuracy)

success = false;

cardK = length(mathcalK);
alphaK = cell(1,cardK);
for i = 1:cardK
    alphaK{i} = alpha(mathcalK{i});
end
beta = mna('beta',1:cardK);

%% ranks and singular values with respect to accuracy
[num_stable_sv_true,prod_stable_sv_true,sigma_true] = eval_this(Xtrue,alphaK,beta,accuracy);
[num_stable_sv,prod_stable_sv,sigma] = eval_this(X,alphaK,beta,accuracy);

%% difference and quotient
diff = num_stable_sv - num_stable_sv_true;
quot = prod_stable_sv / prod_stable_sv_true;

%% residuals
[~,~,rel_nres_y] = net_dist({L,X},y);
[~,~,rel_nres_X_true] = net_dist(X,Xtrue);

%% check whether success (1) or improvement (2)
if rel_nres_y < accuracy
    if diff == 0 && quot > 0.98 && quot < 1.005
        success = true;
    end
    if diff < 0 || diff == 0 && quot <= 0.98
        success = 2;
    end
end

end

function [num_stable_sv,prod_stable_sv,sigma_unfold] = eval_this(X,alphaK,beta,accuracy)

[~,sigma_unfold] = mathcalK_singular_values(X,alphaK,beta);
norm_X = net_norm(X);
num_stable_sv = 0;
prod_stable_sv = 1;
for i = 1:length(sigma_unfold)
    ind_stable = sigma_unfold{i} > accuracy*norm_X;
    num_stable_sv = num_stable_sv + sum(ind_stable);
    prod_stable_sv = prod_stable_sv*prod(sigma_unfold{i}(ind_stable));
end

end

function [U,sigma_unfold] = mathcalK_singular_values(X,alphaK,beta)
% mathcalK singular values

[U,sigma_unfold] = deal(cell(1,length(alphaK)));

for i = 1:length(alphaK)
    [U{i},sigma_i,~] = node_svd(X,alphaK{i},beta{i},-1);
    sigma_unfold{i} = unfold(sigma_i);
end

end