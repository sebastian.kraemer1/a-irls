function [problem,solution] = get_full_tensor_recovery_prob_sol(problem_setting,solution_setting)

n_ = problem_setting.n_values;
d = numel(n_);
alpha = mna('alpha',1:d);

n = assign_mode_size(alpha,n_);

%% sample solution
switch solution_setting.Xtruetype
    case 'CP'
        tau = 'tau';
        n.(tau) = solution_setting.rXtrue;
        
        phi = cell(1,d);
        for i = 1:d
            phi{i} = fold(randn(n.(alpha{i}),solution_setting.rXtrue),{alpha{i},tau},n);
        end
        Xtrue = boxtimes(phi);
        
        ell = problem_setting.ell_fct(n_,solution_setting.rXtrue,data_volume(phi));
        
    case 'HT'
        cardKtrue = length(solution_setting.HTmathcalKtrue);
        alphaKtrue = cell(1,cardKtrue);
        for i = 1:cardKtrue
            alphaKtrue{i} = alpha(solution_setting.HTmathcalKtrue{i});
        end
        
        [~,mNtrue,~,~,beta] = RTLGRAPH(alpha,alphaKtrue,'beta');
        for mn = beta
            n.(mn{1}) = solution_setting.rXtrue; %1+randi(solution_setting.rXtrue-1);
        end
        Ntrue = init_net(mNtrue,n);
        Ntrue = randomize_net(Ntrue);
        Xtrue = boxtimes(Ntrue);
        
        ell = problem_setting.ell_fct(n_,solution_setting.rXtrue,data_volume(Ntrue));
end
ell = ceil(ell);

%% operator L
N = data_volume(Xtrue);
switch problem_setting.Ltype
    case 'gaussian'
        L_ = randn(ell,N);
        
    case 'sampling'
        I = eye(N);
        samps = sort(datasample(1:N,ell,'Replace',false));
        L_ = I(samps,:);
end

zeta = 'zeta';
n.(zeta) = ell;
L = fold(L_,[zeta,alpha],n);

%% y
y = boxtimes(L,Xtrue);

%% transfer
problem = struct;
solution = struct;

problem.n = n;
problem.d = d;
problem.alpha = alpha;
problem.L = L;
problem.y = y;
problem.zeta = zeta;
problem.mathcalK = problem_setting.mathcalK;
problem.ell = ell;

solution.Xtrue = Xtrue;

end
